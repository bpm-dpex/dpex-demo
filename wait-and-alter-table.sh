#!/bin/bash

# Database configuration
HOST='localhost' # Since the script runs inside the container, use localhost
USER='root' # Using root user, as defined Docker environment variables
PASSWORD='test' # Password for root user, as per Docker Compose environment
DATABASE='keycloak_schema' # Target database

# Function to check if modifications are already applied
check_modifications() {
    # Check the column type of VALUE
    local column_type=$(mysql -h "$HOST" -u "$USER" -p"$PASSWORD" -N -B -e "SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$DATABASE' AND TABLE_NAME = 'USER_ATTRIBUTE' AND COLUMN_NAME = 'VALUE';")

    # Check for the existence and type of the index on the VALUE column
    local index_exists=$(mysql -h "$HOST" -u "$USER" -p"$PASSWORD" -N -B -e "SELECT COUNT(*) FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = '$DATABASE' AND TABLE_NAME = 'USER_ATTRIBUTE' AND COLUMN_NAME = 'VALUE' AND INDEX_NAME = 'IDX_USER_ATTRIBUTE_NAME';")

    if [[ "$column_type" == "text" ]] && ((index_exists > 0)); then
        return 0 # Modifications are applied
    else
        return 1 # Modifications not applied
    fi
}

# Initial check to see if modifications are already applied
if check_modifications; then
    echo "Database schema modifications already applied. Exiting..."
    exit 0
fi

# Wait for the USER_ATTRIBUTE table and its schema to be fully initialized
echo "Waiting for USER_ATTRIBUTE table and its schema to be fully initialized in $DATABASE..."

while true; do
    # Check if the USER_ATTRIBUTE table exists
    if mysql -h "$HOST" -u "$USER" -p"$PASSWORD" -e "USE $DATABASE; SHOW TABLES LIKE 'USER_ATTRIBUTE';" | grep -q 'USER_ATTRIBUTE'; then
        echo "USER_ATTRIBUTE table found. Checking column type and index..."

        # Check the column type of VALUE
        COLUMN_TYPE=$(mysql -h "$HOST" -u "$USER" -p"$PASSWORD" -N -B -e "SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$DATABASE' AND TABLE_NAME = 'USER_ATTRIBUTE' AND COLUMN_NAME = 'VALUE';")

        # Check the existence and type of the index on the VALUE column
        INDEX_TYPE=$(mysql -h "$HOST" -u "$USER" -p"$PASSWORD" -N -B -e "SELECT INDEX_TYPE FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = '$DATABASE' AND TABLE_NAME = 'USER_ATTRIBUTE' AND COLUMN_NAME = 'VALUE' AND INDEX_NAME = 'IDX_USER_ATTRIBUTE_NAME';")

        if [[ "$COLUMN_TYPE" == *"varchar(255)"* ]] && [[ "$INDEX_TYPE" != "" ]]; then
            echo "USER_ATTRIBUTE schema is fully initialized. Proceeding with modifications..."
            break
        else
            echo "USER_ATTRIBUTE schema is not fully initialized. Retrying in 60 seconds..."
            sleep 60
        fi
    else
        echo "USER_ATTRIBUTE table not found. Retrying in 60 seconds..."
        sleep 60
    fi
done

COLUMN_TYPE=$(mysql -h "$HOST" -u "$USER" -p"$PASSWORD" -N -B -e "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$DATABASE' AND TABLE_NAME = 'USER_ATTRIBUTE' AND COLUMN_NAME = 'VALUE';")

if [ "$COLUMN_TYPE" != "text" ]; then
    echo "VALUE column is not of type TEXT. Altering column type..."
    # Execute ALTER TABLE commands to modify the schema as required
    mysql -h "$HOST" -u "$USER" -p"$PASSWORD" $DATABASE <<EOF
ALTER TABLE USER_ATTRIBUTE DROP INDEX IDX_USER_ATTRIBUTE_NAME;
ALTER TABLE USER_ATTRIBUTE MODIFY COLUMN VALUE TEXT;
CREATE INDEX IDX_USER_ATTRIBUTE_NAME ON USER_ATTRIBUTE(VALUE(255));
EOF
    echo "Modifications to USER_ATTRIBUTE table completed."
else
    echo "VALUE column is already of type TEXT. No alterations needed."
fi
