#!/bin/bash

# Start MySQL in the background
/usr/local/bin/docker-entrypoint.sh mysqld &

# Wait for MySQL to start
echo "Waiting for MySQL to start..."
sleep 30

# Now the custom script
echo "Running custom script..."
/usr/local/bin/wait-and-alter-table.sh

# Keep the container running
wait
