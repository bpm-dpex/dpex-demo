package ubt.ai4.dpexdemo.camunda.org;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Entity;
import javax.persistence.Transient;
import dpex.action.Action;
import dpex.action.TaskAction;
import dpex.bpm.execution.Instance;
import dpex.bpm.execution.Task;
import dpex.bpm.model.ProcessModel;
import dpex.core.User;
import dpex.error.TaskNotFoundException;
import dpex.error.UnknownProcessModelException;
import dpex.impl.bpm.camunda.CamundaExecutionEngine;
import dpex.impl.bpm.model.bpmn.BPMNModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ubt.ai4.dpexdemo.org.OrgEngine;


/**
 *
 * Mapping namespaces DPEX -> Camunda:
 *  ModelReference                       -> Definition Key
 *  [not used] (local ModelReference)    -> Definition ID
 *  Instance.localInstanceReference      -> processInstanceId
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class CamundaBPMEngine extends CamundaExecutionEngine {

    @Transient
    private static final OrgEngine orgEngine = new OrgEngine();

    @Override
    public Instance instantiate(String modelReference, String businessKey, String JSONVariables, String name) throws UnknownProcessModelException {
        return super.instantiate(modelReference, businessKey, JSONVariables, name);
    }

    /**
     * Check if the given task is listed in the work list
     * @param instance
     * @param taskAction
     * @return
     */
    @Override
    public boolean isConform(ProcessModel processModel, Instance instance, TaskAction taskAction) {

        CamundaMPModel camundaMPModel = (CamundaMPModel) processModel;

        BPMNModel model = BPMNModel.fromString(new String(camundaMPModel.getBpmnModel()));

        //CHECK ORG CONFORM
        boolean orgConform = orgEngine.evaluate(model, camundaMPModel.getOrganizationalModel(), instance.getEventLog(), taskAction);

        //CHECK CONTROL FLOW CONFORM
        boolean controlFlowConform = super.isConform(processModel, instance, taskAction);

        return orgConform && controlFlowConform;
    }

    @Override
    public void claim(Instance instance, TaskAction taskAction) throws TaskNotFoundException {
        super.claim(instance, taskAction);
    }

    /**
     * In camunda, the task id is unique over all instances. The instance id is not used.
     *
     * @param instance
     * @param action
     */
    @Override
    public void execute(Instance instance, Action action) throws TaskNotFoundException {
      super.execute(instance, action);
    }

    @Override
    public List<Task> getWorkList(Instance instanceReference) {
     return super.getWorkList(instanceReference);
    }

    @Override
    public List<Task> getWorkListByUser(Instance instanceReference, String userReference) {
        return super.getWorkListByUser(instanceReference, userReference);
    }

    /**
     * Returns all tasks that a given user is currently eligible to execute
     */
    @Override
    public List<Task> getIndividualWorkList(ProcessModel processModel, Instance instance, String bpmId) {
    	CamundaMPModel camundaMPModel = (CamundaMPModel) processModel;
        BPMNModel model = BPMNModel.fromString(new String(camundaMPModel.getBpmnModel()));
		//all tasks that have been claimed by the user
		List<Task> worklist = getWorkListByUser(instance, bpmId);
		//all tasks that can be executed now but are unclaimed
		List<Task> unclaimedTasks = getWorkList(instance).stream().filter(task -> task.getResource() == null).collect(Collectors.toList());
		List<Task> conformTasks = new ArrayList<Task>();
		for(Task task : unclaimedTasks) {
			User user = User.builder().bpmId(bpmId).build();
			TaskAction taskAction = TaskAction.builder().task(task).user(user).build();
			if(orgEngine.evaluate(model, camundaMPModel.getOrganizationalModel(), instance.getEventLog(), taskAction))
				conformTasks.add(task);
		}
		conformTasks.addAll(worklist);
		return conformTasks;
	}

}







