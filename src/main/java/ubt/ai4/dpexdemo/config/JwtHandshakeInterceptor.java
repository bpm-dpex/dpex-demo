package ubt.ai4.dpexdemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

/**
 * An interceptor for WebSocket handshake requests that validates JWT tokens.
 * Since WebSocket handshake requests can typically not include authorization headers,
 * this interceptor extracts the token from URL parameters and uses a {@link JwtDecoder}
 * to validate it. If the token is invalid, the handshake is aborted, ensuring that only
 * authenticated users can establish WebSocket connections.
 */
@Component
public class JwtHandshakeInterceptor implements HandshakeInterceptor {

    private final JwtDecoder jwtDecoder;

    /**
     * Constructs a JwtHandshakeInterceptor with the specified {@link JwtDecoder}.
     *
     * @param jwtDecoder the JwtDecoder used for validating JWT tokens.
     */
    @Autowired
    public JwtHandshakeInterceptor(JwtDecoder jwtDecoder) {
        this.jwtDecoder = jwtDecoder;
    }

    /**
     * Intercepts and validates the WebSocket handshake request. The JWT token is extracted from
     * the URL query parameters and validated. If the token is valid, the handshake process proceeds;
     * otherwise, it is aborted.
     *
     * @param request    The server HTTP request for the handshake.
     * @param response   The server HTTP response for the handshake.
     * @param wsHandler  The WebSocket handler for the connection.
     * @param attributes A map of attributes to the WebSocket session.
     * @return true if the handshake should proceed, false otherwise.
     * @throws Exception if an error occurs during token validation.
     */
    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
        String token = getTokenFromRequest(request);
        if (token != null && !token.isEmpty()) {
            try {
                jwtDecoder.decode(token);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }


    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {
        // Method stub. No action taken after handshake.
    }

    /**
     * Extracts the JWT token from the URL query parameters of the handshake request.
     *
     * @param request The server HTTP request for the handshake.
     * @return The JWT token if present in the request URL; null otherwise.
     */
    private String getTokenFromRequest(ServerHttpRequest request) {
        String query = request.getURI().getQuery();
        if (query != null) {
            String[] params = query.split("&");
            for (String param : params) {
                if (param.startsWith("token=")) {
                    return param.split("=")[1];
                }
            }
        }
        return null;
    }

}
