package ubt.ai4.dpexdemo.config;

import dpex.db.TenantContext;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import ubt.ai4.dpexdemo.multitenancy.TenantAwareIssuerService;
import ubt.ai4.dpexdemo.multitenancy.TenantIdentificationFilter;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

/**
 * Security configuration class for the application. It sets up JWT token validation, CORS policies,
 * and method-level security annotations. This configuration also includes a custom JWT authentication
 * converter and dynamically adjusts security settings based on the application's environment profile,
 * providing stricter access controls in production while allowing more open access in development settings.
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {
    private final JwtAuthConverter jwtAuthConverter;
    private final TenantAwareIssuerService issuerService;
    @Autowired(required = false)
    private TenantIdentificationFilter tenantIdentificationFilter;

    @Autowired
    private Environment env;

    /**
     * Creates a {@link JwtDecoder} bean that is tenant-aware. It dynamically determines
     * the issuer URI and JWK set URI based on the tenant ID from the request context.
     *
     * @return A {@link JwtDecoder} capable of decoding JWTs dynamically for multiple tenants.
     */
    @Bean
    public JwtDecoder jwtDecoder() {
        return token -> {
            String tenantId = TenantContext.getTenantId();
            String issuerUri = issuerService.getIssuerUri(tenantId);
            String jwkSetUri = issuerService.getJwkSetUri(tenantId);

            NimbusJwtDecoder jwtDecoder = NimbusJwtDecoder.withJwkSetUri(jwkSetUri).build();

            return jwtDecoder.decode(token);
        };
    }

    /**
     * Configures CORS settings for the application, allowing requests from specified origins
     * and configuring headers and methods that are accepted by the CORS policy.
     *
     * @param allowedOrigin The allowed origin for CORS requests.
     * @return A {@link CorsFilter} configured with the application's CORS policy.
     */
    @Bean
    public CorsFilter corsFilter(@Value("${app.cors.allowed-origin}") String allowedOrigin) {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Arrays.asList(allowedOrigin));
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        config.setAllowedHeaders(Arrays.asList("Authorization", "Content-Type", "X-Communication-Token"));
        config.setExposedHeaders(Arrays.asList("Authorization"));
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

    /**
     * Defines a custom {@link org.springframework.security.web.util.matcher.RequestMatcher}
     * that matches requests intended for the internal Camunda engine, allowing them through the security filter if they
     * are coming from the localhost to the App itself.
     */
    class CamundaEngineRestMatcher implements RequestMatcher {
        @Override
        public boolean matches(HttpServletRequest request) {
            // Check if the request is coming from the app and targeted at local camunda engine
            String remoteAddr = request.getRemoteAddr();
            String requestURI = request.getRequestURI();
            return ("/engine-rest".equals(requestURI) || requestURI.startsWith("/engine-rest/")) &&
                    ("127.0.0.1".equals(remoteAddr) || "::1".equals(remoteAddr));
        }
    }

    /**
     * Sets up security filters and rules for the application.
     * This includes setting up the JWT token validation (includes e.g. the custom filter that extracts tenant context
     * information important to token validation before the actual authentication is happening)
     * , CORS configuration, and specifying which requests are permitted or secured. The configuration varies based on
     * active Spring profiles, allowing for different security settings in development and production environments.
     *
     * @param http The {@link HttpSecurity} object to configure.
     * @return A {@link SecurityFilterChain} that represents the configured security settings.
     * @throws Exception If there is an error in the configuration.
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        List<String> activeProfiles = Arrays.asList(env.getActiveProfiles());

        http
                .cors().and()
                .csrf().disable();

        if (!activeProfiles.contains("dev") && !activeProfiles.contains("test")) {
            // Non-dev configurations
            http
                    .addFilterBefore(tenantIdentificationFilter, UsernamePasswordAuthenticationFilter.class)
                    // Permit all requests from localhost for internal requests to the local camunda engine
                    .authorizeRequests()
                    .requestMatchers(new CamundaEngineRestMatcher()).permitAll()
                    .and()
                    // Configuration for other requests
                    .authorizeRequests()
                    .antMatchers("/dpex-ws").permitAll()
                    .antMatchers("/api/tenants/all-tenants").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .oauth2ResourceServer()
                    .jwt()
                    .jwtAuthenticationConverter(jwtAuthConverter);
        } else {
            // Dev configurations
            http.authorizeRequests()
                    .anyRequest().permitAll();
        }

        http.sessionManagement()
                .sessionCreationPolicy(STATELESS);

        return http.build();
    }
}
