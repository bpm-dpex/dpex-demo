package ubt.ai4.dpexdemo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * A utility component that provides static methods for performing security checks,
 * such as verifying BPM IDs, checking if the JWT issuer is the master realm, and
 * validating admin roles based on JWT claims. Can later be used in @PreAuthorize Annotations.
 */
@Component("securityUtils")
public class SecurityUtils {
    private static String baseUrl;
    private static String masterRealmName;

    @Value("${tenant.base-url}")
    public void setBaseUrl(String baseUrl) {
        SecurityUtils.baseUrl = baseUrl;
    }

    @Value("${tenant.master-realm-name}")
    public void setMasterRealmName(String masterRealmName) {
        SecurityUtils.masterRealmName = masterRealmName;
    }


    /**
     * Checks if the current authentication contains a specific BPM ID in the custom authentication details.
     *
     * @param bpmId The BPM ID to check.
     * @return true if the BPM ID is present; false otherwise.
     */
    public static boolean hasBpmId(String bpmId) {
        ExtendedJwtAuthenticationToken authentication = (ExtendedJwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getCustomAuthDetails() != null) {
            List<String> bpmIds = authentication.getCustomAuthDetails().getBpmIds();
            return bpmIds.contains(bpmId);
        }
        return false;
    }

    /**
     * Checks if the JWT issuer of the current authentication matches the master realm.
     *
     * @return true if the JWT issuer is the master realm; false otherwise.
     */
    public static boolean isMasterRealm() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof JwtAuthenticationToken) {
            Jwt jwt = ((JwtAuthenticationToken) authentication).getToken();
            String issuerUri = jwt.getIssuer().toString();
            return issuerUri.equals(baseUrl + masterRealmName);
        }
        return false;
    }

    /**
     * Verifies if the current authentication has an 'admin' role in the master realm.
     *
     * @return true if the current user has an 'admin' role; false otherwise.
     */
    public static boolean checkIfUserIsGlobalAdminInSaaSSystem() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof JwtAuthenticationToken) {
            Jwt jwt = ((JwtAuthenticationToken) authentication).getToken();
            Map<String, Object> resourceAccess = jwt.getClaim("resource_access");

            if (resourceAccess != null && resourceAccess.containsKey(masterRealmName + "-realm")) {
                Map<String, Object> realmAccess = jwt.getClaim("realm_access");
                if (realmAccess != null) {
                    List<String> roles = (List<String>) realmAccess.get("roles");
                    return roles.contains("admin");
                }
            }
        }
        return false;
    }
}
