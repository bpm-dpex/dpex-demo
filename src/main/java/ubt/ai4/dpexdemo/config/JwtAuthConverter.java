package ubt.ai4.dpexdemo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import dpex.db.TenantContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimNames;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Converts a JWT token to a customized {@link ExtendedJwtAuthenticationToken} that
 * includes additional authentication details extracted from the JWT. This converter
 * is designed to extract the tenant-context and custom claims, such as roles
 * and BPM IDs.
 */
@Component
public class JwtAuthConverter implements Converter<Jwt, AbstractAuthenticationToken> {

    private final JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter =
            new JwtGrantedAuthoritiesConverter();

    @Value("${jwt.auth.converter.principle-attribute}")
    private String principleAttribute;
    @Value("${jwt.auth.converter.resource-id}")
    private String resourceId;

    /**
     * Extracts the tenant ID from the JWT issuer claim. If the tenant ID is not found,
     * a default value is returned.
     *
     * @param jwt The JWT from which to extract the tenant ID.
     * @return The extracted tenant ID or a default value if not found.
     */
    private static String extractTenantId(Jwt jwt) {
        String issuer = jwt.getClaimAsString("iss");
        try {
            URI issuerUri = new URI(issuer);
            String path = issuerUri.getPath();
            String tenantId = path.substring(path.lastIndexOf('/') + 1);
            return tenantId.isEmpty() ? "dpex_demo_schema" : tenantId; // Use default in case of error
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to parse issuer URI", e);
        }
    }

    /**
     * Converts a JWT token into an {@link ExtendedJwtAuthenticationToken} with enhanced
     * authorities and custom authentication details. This method extracts additional roles
     * from the JWT and integrates tenant context and BPM IDs into the security context.
     *
     * @param jwt The JWT token to convert.
     * @return An {@link ExtendedJwtAuthenticationToken} enriched with custom authentication details.
     */
    @Override
    public AbstractAuthenticationToken convert(@NonNull Jwt jwt) {
        String tenantId = extractTenantId(jwt);
        TenantContext.setTenantId(tenantId);

        Collection<GrantedAuthority> authorities = Stream.concat(
                jwtGrantedAuthoritiesConverter.convert(jwt).stream(),
                extractResourceRoles(jwt).stream()
        ).collect(Collectors.toSet());


        // Extract bpm_IDs and set as details using the custom object
        CustomAuthDetails customAuthDetails = null;
        String bpmIdsJson = jwt.getClaim("accountDetails");
        if (bpmIdsJson != null) {
            try {
                // Parse the JSON array into a list of objects
                List<Map<String, Object>> bpmIdObjects = new ObjectMapper().readValue(bpmIdsJson, new TypeReference<List<Map<String, Object>>>() {
                });
                // Extract the bpmId values into a List<String>
                List<String> bpmIds = bpmIdObjects.stream()
                        .map(obj -> obj.get("bpmId").toString())
                        .collect(Collectors.toList());
                customAuthDetails = new CustomAuthDetails(bpmIds);
            } catch (Exception e) {
                throw new RuntimeException("Could not parse bpm_IDs: " + e);
            }
        }

        return new ExtendedJwtAuthenticationToken(
                jwt,
                authorities,
                getPrincipleClaimName(jwt),
                customAuthDetails
        );
    }


    /**
     * Extracts the principal claim name from the JWT token based on configured attributes.
     *
     * @param jwt The JWT token from which to extract the principal claim name.
     * @return The principal's name.
     */
    private String getPrincipleClaimName(Jwt jwt) {
        String claimName = JwtClaimNames.SUB;
        if (principleAttribute != null) {
            claimName = principleAttribute;
        }
        return jwt.getClaim(claimName);
    }

    /**
     * Extracts resource roles from the JWT token based on the configured resource ID.
     *
     * @param jwt The JWT token from which to extract resource roles.
     * @return A collection of granted authorities based on the extracted roles.
     */
    private Collection<? extends GrantedAuthority> extractResourceRoles(Jwt jwt) {
        Map<String, Object> resourceAccess;
        Map<String, Object> resource;
        Collection<String> resourceRoles;
        if (jwt.getClaim("resource_access") == null) {
            return Set.of(); // Return empty Set
        }
        resourceAccess = jwt.getClaim("resource_access");

        if (resourceAccess.get(resourceId) == null) {
            return Set.of();
        }
        resource = (Map<String, Object>) resourceAccess.get(resourceId);

        resourceRoles = (Collection<String>) resource.get("roles");
        return resourceRoles
                .stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                .collect(Collectors.toSet());
    }
}