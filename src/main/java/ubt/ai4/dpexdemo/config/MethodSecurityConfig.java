package ubt.ai4.dpexdemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class to enable and customize method-level security within the application.
 * Method security allows for the use of annotations like {@code @PreAuthorize} to secure
 * individual methods based on authorization logic (e.g. the methods implemented in SecurityUtils).
 * This class is only active in non-test and non-development profiles, ensuring that method-level security
 * checks are applied in production.
 */
@Profile({"!test & !dev"})
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {

    private final ApplicationContext context;

    /**
     * Constructs a new MethodSecurityConfig instance, injecting the application context
     * to allow for custom security expression handling within method security annotations.
     *
     * @param context The application context, providing access to application beans and environment.
     */
    @Autowired
    public MethodSecurityConfig(ApplicationContext context) {
        this.context = context;
    }

    /**
     * Customizes the {@link MethodSecurityExpressionHandler} used by the security framework
     * to evaluate security expressions in method-level security annotations. This implementation
     * sets the application context on the default expression handler, enabling custom security utilities
     * to be invoked directly from security annotations (e.g., {@code @PreAuthorize("@securityUtils.checkAdminRole()")}).
     *
     * @return A customized {@link MethodSecurityExpressionHandler} configured with the application context.
     */
    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
        expressionHandler.setApplicationContext(context);
        return expressionHandler;
    }
}
