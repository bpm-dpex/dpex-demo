package ubt.ai4.dpexdemo.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Collection;

import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;


/**
 * An extension of JwtAuthenticationToken to include custom authentication details, specifically
 * for storing additional information extracted from JWT tokens.
 */
public class ExtendedJwtAuthenticationToken extends JwtAuthenticationToken {
    private final CustomAuthDetails customAuthDetails;

    /**
     * Constructs an ExtendedJwtAuthenticationToken with JWT, authorities, principal name, and
     * custom authentication details (currently only BPM IDs).
     *
     * @param jwt               The Jwt token.
     * @param authorities       The collection of granted authorities.
     * @param principal         The principal name.
     * @param customAuthDetails The custom authentication details to be associated with this token.
     */
    public ExtendedJwtAuthenticationToken(Jwt jwt, Collection<? extends GrantedAuthority> authorities, String principal, CustomAuthDetails customAuthDetails) {
        super(jwt, authorities, principal);
        this.customAuthDetails = customAuthDetails;
    }

    /**
     * Retrieves the custom authentication details associated with this token.
     *
     * @return The CustomAuthDetails object containing additional information like BPM IDs.
     */
    public CustomAuthDetails getCustomAuthDetails() {
        return customAuthDetails;
    }
}
