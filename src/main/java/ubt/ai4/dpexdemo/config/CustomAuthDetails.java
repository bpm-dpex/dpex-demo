package ubt.ai4.dpexdemo.config;

import java.util.List;

/**
 * A utility class used to encapsulate additional information (e.g., BPM IDs)
 * that can be added to JWT tokens after they have been decoded. This class
 * allows for the storage and retrieval of BPM IDs which are critical for
 * performing additional security checks and validations.
 */
public class CustomAuthDetails {
    private List<String> bpmIds;

    /**
     * Constructs a new CustomAuthDetails object with the specified BPM IDs.
     *
     * @param bpmIds A list of BPM IDs to be associated with this authentication details.
     */
    public CustomAuthDetails(List<String> bpmIds) {
        this.bpmIds = bpmIds;
    }

    /**
     * Retrieves the list of BPM IDs associated with this authentication details.
     *
     * @return A list of BPM IDs.
     */
    public List<String> getBpmIds() {
        return bpmIds;
    }

    /**
     * Sets the list of BPM IDs for this authentication details.
     *
     * @param bpmIds A list of BPM IDs to be associated with this authentication details.
     */
    public void setBpmIds(List<String> bpmIds) {
        this.bpmIds = bpmIds;
    }
}

