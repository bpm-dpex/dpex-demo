package ubt.ai4.dpexdemo.utils;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import dpex.action.TaskAction;
import dpex.bpm.execution.Instance;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.bpm.model.ProcessModel;
import dpex.core.Alliance;
import dpex.core.User;
import dpex.db.AllianceRepository;
import dpex.error.TaskNotFoundException;
import dpex.events.InstantiationEvent;
import dpex.impl.bpm.model.bpmn.Activity;
import dpex.impl.bpm.model.bpmn.BPMNModel;
import lombok.RequiredArgsConstructor;
import ubt.ai4.dpexdemo.camunda.org.CamundaBPMEngine;
import ubt.ai4.dpexdemo.camunda.org.CamundaMPModel;

import java.io.*;
import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
public class ScriptExecutor {

    private final AllianceRepository allianceRepository;

    private  BPMNModel getBPMNModel(Alliance alliance){
        ProcessModel processModel = alliance.getModel();
        CamundaMPModel camundaMPModel = (CamundaMPModel) processModel;
        BPMNModel bpmnModel = BPMNModel.fromString(new String(camundaMPModel.getBpmnModel()));
        return bpmnModel;
    }

    private Activity getActivityByTask(BPMNModel bpmnModel, Task task){
        Activity activity = null;
        try{
            activity = bpmnModel.getActivities().stream().filter(a -> a.getName().equals(task.getActivity())).findFirst().get();
        }catch(NoSuchElementException e){
            return null;
        }
        return activity;
    }

    private boolean isScriptTask(Activity activity){
        if(activity == null){
            return false;
        }
        String annotation = activity.getAnnotation();
        if(annotation == null){
            return false;
        }

        int startScriptAnnotIndex = annotation.indexOf("script(");
        if(startScriptAnnotIndex == -1){
            return false;
        }
        return true;
    }

    private void executeScript(Activity activity, String modelName){

        //startScriptAnnotIndex should never be -1 because of the context in which this function gets called. We still check for it
        int startScriptAnnotIndex = activity.getAnnotation().indexOf("script(");
        if(startScriptAnnotIndex == -1) return;

        int endScriptAnnotIndex = activity.getAnnotation().substring(startScriptAnnotIndex+1).indexOf(")");
        String scriptName = activity.getAnnotation().substring(startScriptAnnotIndex+7, endScriptAnnotIndex+1);

        Process process = null;
        try {
            //Here the taskId would have to be added
            process = new ProcessBuilder("python3", "./scripts/" + modelName + "/" + scriptName).start();
        } catch (IOException e) {
            throw new RuntimeException("A script which was supposed to be executed couldn't be found in the local script folder");
        }
        int exitCode = 0;
        try {
            exitCode = process.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException("Executing a script of an automated task was interrupted");
        }
        //Here you could check whether the execution was successful. If it wasn't you could add a matching reaction
        return;
    }

    private Alliance getAlliance(String globalInstanceReference){
        return allianceRepository.findByGlobalInstanceReference(globalInstanceReference).get();
    }

    /**
     * Checks for scripttasks using a TaskAction (its supposed to be called after a complete call)
     * @param taskAction
     * @throws TaskNotFoundException
     */
    public void manageScripts(TaskAction taskAction) throws TaskNotFoundException {
        Alliance alliance = getAlliance(taskAction.getInstance().getGlobalInstanceReference());
        CamundaBPMEngine engine = (CamundaBPMEngine) alliance.getEngine();
        BPMNModel bpmnModel = getBPMNModel(alliance);
        Instance instance = taskAction.getInstance();

        try {
            manageScripts(engine, bpmnModel, instance);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks for scripttasks using an instantiationevent (its supposed to be called after an instantiation)
     * @param event
     * @throws TaskNotFoundException
     */
    public void manageScripts(InstantiationEvent event) throws TaskNotFoundException {
        Alliance alliance = getAlliance(event.getInstance().getGlobalInstanceReference());
        CamundaBPMEngine engine = (CamundaBPMEngine) alliance.getEngine();
        BPMNModel bpmnModel = getBPMNModel(alliance);
        Instance instance = event.getInstance();

        try {
            manageScripts(engine, bpmnModel, instance);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void manageScripts(CamundaBPMEngine engine, BPMNModel bpmnModel, Instance instance) throws TaskNotFoundException, IOException {
        List<Task> nextTasks = engine.getWorkList(instance);

        //Check for every task whether it is a script task and if so execute the matching script
        int i = 0;
        while(i < nextTasks.size()){
            Task t = nextTasks.get(i);
            Activity activity = getActivityByTask(bpmnModel, t);
            if(isScriptTask(activity)){
                //claim the task before
                ProcessVariables claimVariables = engine.getInstanceVariables(instance.getLocalInstanceReference());
                TaskAction newClaimTaskAction = new TaskAction(instance, t, claimVariables, TaskLifeCycleStage.CLAIM);
                User user = new User();
                user.setBpmId("localUserForScriptExecution");
                newClaimTaskAction.setUser(user);
                engine.claim(instance, newClaimTaskAction);

                //execute the script (do the task)
                //Because the script execution takes a little time the claim
                //should always be done by the end of the execution so that the
                //complete takes place without problems
                Alliance alliance = allianceRepository.findByGlobalInstanceReference(instance.getGlobalInstanceReference()).get();
                String modelReference = alliance.getModel().getModelReference();
                executeScript(activity, modelReference);

                //complete the task afterwards
                ProcessVariables completeVariables = engine.getInstanceVariables(instance.getLocalInstanceReference());
                TaskAction newCompleteTaskAction = new TaskAction(instance, t, completeVariables, TaskLifeCycleStage.COMPLETE);
                newCompleteTaskAction.setUser(user);
                engine.execute(instance, newCompleteTaskAction);

                //Refresh tasks because now there might be tasks available which weren't before
                //set i = 0 to make sure no tasks are being skipped (for example when using gateways
                nextTasks = engine.getWorkList(instance);
                i = 0;
            }else{
                i++;
            }
        }
    }
}
