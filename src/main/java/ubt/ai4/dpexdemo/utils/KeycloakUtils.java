package ubt.ai4.dpexdemo.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

public class KeycloakUtils {

    /**
     * Determines if a given string is a valid UUID.
     *
     * @param value The string to be checked.
     * @return true if the string is a valid UUID; false otherwise.
     */
    public static boolean isUuid(String value) {
        try {
            UUID.fromString(value);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    /**
     * Loads a JSON template from the classpath. This method is typically used for reading realm configuration
     * templates stored as JSON files within the application resources.
     *
     * @param resourceLoader The ResourceLoader provided by Spring, capable of accessing classpath resources.
     * @param resourcePath The path to the resource file within the classpath.
     * @return The content of the JSON template as a String.
     * @throws IOException If the file cannot be read.
     */
    public static String loadJsonTemplate(ResourceLoader resourceLoader, String resourcePath) throws IOException {
        Resource resource = resourceLoader.getResource("classpath:" + resourcePath);
        try (InputStream inputStream = resource.getInputStream()) {
            return new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        }
    }
}
