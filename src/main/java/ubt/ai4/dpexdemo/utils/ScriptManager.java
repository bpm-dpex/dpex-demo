package ubt.ai4.dpexdemo.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class ScriptManager {
    public static void saveFile(MultipartFile file, String modelName) throws IOException {
        createModelDirectory(modelName);
        String fileName = "scripts/" + modelName + "/" + file.getOriginalFilename();
        File scriptFile = new File(fileName);
        FileOutputStream fos = new FileOutputStream(scriptFile, false);
        InputStream inStream = file.getInputStream();
        fos.write(inStream.readAllBytes());
        inStream.close();
        fos.close();
    }

    /**
     * Creates the model directory if it does not exist
     * @param modelName
     */
    public static void createModelDirectory(String modelName){
        String modelScriptDirPath = "scripts/" + modelName;
        File dir = new File(modelScriptDirPath);
        if(!(dir.exists() && dir.isDirectory())){
            dir.mkdir();
        }
    }
}
