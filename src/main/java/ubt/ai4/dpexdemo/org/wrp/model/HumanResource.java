package ubt.ai4.dpexdemo.org.wrp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HumanResource {

    @Id
    private String name;

    @ManyToOne
    Position occupies;

}
