package ubt.ai4.dpexdemo.org.wrp.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ubt.ai4.dpexdemo.org.wrp.model.OrganizationalModel;

@Repository
public interface OrganizationalModelRepository extends JpaRepository<OrganizationalModel, Long> {
}
