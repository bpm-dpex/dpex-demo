package ubt.ai4.dpexdemo.org;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import dpex.bpm.execution.EventLog;
import dpex.bpm.model.ProcessModel;
import dpex.core.Event;
import dpex.core.User;
import dpex.impl.bpm.model.bpmn.Activity;
import dpex.impl.bpm.model.bpmn.BPMNModel;

public class OrgUtils {
	//when using the frontend split on "\r\n", when using postman only "\n"
	public static final String lineSplitter = "\r\n";
	
	//defining the structure of the org model
	public static final int bpmIdIndex = 0;
	public static final int sciIdIndex = 1;
	public static final int communicationIdIndex = 2;
	public static final int homeserverUrlIndex = 3;
	public static final int rolesIndex = 4;
	public static final int departmentsIndex = 5;	
	
	/**
     * Retrieves the user object for a given bpm ID
     * @param processModel the process model defining the participants
     * @param bpmId the bpm ID of the user to search for
     * @return the corresponding user object
     */
    public static User getUserFromBpmId(ProcessModel processModel, String bpmId) {
    	Set<User> allUsersOfAlliance = processModel.getParticipants();
    	User user = allUsersOfAlliance.stream().filter(user_ -> user_.getBpmId().equals(bpmId)).findFirst().orElseThrow();
    	return user;
    }
	
	/**
	 * Converts an sciId to the corresponding bpmId
	 * @param organizationalModel the organizational model to retrieve the information from
	 * @param sciId the sci ID to be converted
	 * @return the corresponding bpmId
	 */
	public static String getBpmIdFromSciId(String organizationalModel, String sciId) {
        Map<String, String> sciIdToUserReference = Arrays.stream(organizationalModel.split(lineSplitter)).map(line -> parseOrganizationalModel(line, sciIdIndex, bpmIdIndex))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return sciIdToUserReference.get(sciId.toLowerCase());
    }

    private static Map.Entry<String, String> parseOrganizationalModel(String s, int keyIndex, int valueIndex) {
        String[] parts = s.split(","); // Trennt den String an der ersten Vorkommnis von ','
        if (parts.length >= 2) {
            return new AbstractMap.SimpleEntry<>(parts[keyIndex].toLowerCase(), parts[valueIndex]);
        } else {
            throw new IllegalArgumentException("Invalid string: " + s);
        }
    }
    
    /**
     * Retrieves the bpm IDs of the participants that match the given organizational constraints
     * If no organizational constraints are given, all bpm IDs from the org-model will be returned
	 * @param organizationalModel the organizational model to retrieve the information from
     * @param roles the roles of which the participants shall have at least one. If null, the method won't filter by roles
     * @param departments the departments of which the participants shall be member of at least one. If null, the method won't filter by departments
     * @return an array containing all bpm IDs fulfilling the organizational constraints
     */
    public static String[] getBpmIdsByQuery(String organizationalModel, String[] roles, String[] departments) {
    	String[] allLines = organizationalModel.split(lineSplitter);
        //omitting the first line since it's the column names
        String[] lines = Arrays.copyOfRange(allLines, 1, allLines.length);
    	if(roles != null && roles.length > 0)
    		lines = Arrays.stream(lines).filter(line ->
    			haveItemInCommon(
    			    line.split(",")[rolesIndex].split("\\|"),
    			    roles
    			)
    		).toArray(String[]::new);
    	if(departments != null && departments.length > 0)
    		lines = Arrays.stream(lines).filter(line ->
    		    haveItemInCommon(
    		        line.split(",")[departmentsIndex].split("\\|"),
    		        departments
    		    )
    		).toArray(String[]::new);
    	String[] bpmIds = new String[lines.length];
    	for(int i = 0; i < lines.length; i++)
    		bpmIds[i] = lines[i].split(",")[bpmIdIndex];
    	return bpmIds;
    }
    
    private static boolean haveItemInCommon(String[] list1, String[] list2) {
    	for(String item1 : list1)
    		for(String item2 : list2)
    			if(item1.equals(item2))
    				return true;
    	return false;
    }
    
    /**
     * Parses the organizational model and retrieves an array of all participants
     * where each participant is represented as an array of the following structure:
     * [bpmId, communicationId, roles, departments]
     * @param organizationalModel the organizational model to parse
     * @return the described array
     */
    public static ParticipantDTO[] getAllParticipantsWithRolesAndDepartments(String organizationalModel) {
    	String[] allLines = organizationalModel.split(lineSplitter);
        //omitting the first line since it's the column names
        String[] lines = Arrays.copyOfRange(allLines, 1, allLines.length);
        ParticipantDTO[] participants = new ParticipantDTO[lines.length];
        for(int i = 0; i < lines.length; i++) {
        	String line = lines[i];
        	String[] columns = line.split(",");
        	participants[i] = new ParticipantDTO(columns[bpmIdIndex], columns[communicationIdIndex], columns[rolesIndex], columns[departmentsIndex]);
        }
        return participants;
    }
    
    /**
     * Returns the BPM IDs of the participants that are eligible to execute a given task according to the organizational constraints specified in the process model.
     * @param task the task for which the potential task executors shall be retrieved
     * @param log the event log
     * @param organizationalModel
     * @param bpmnModel
     * @return array of BPM IDs
     */
    public static String[] getPotentialTaskExecutors(String task, EventLog log, String organizationalModel, BPMNModel bpmnModel) {
        Activity activity = bpmnModel.getActivities().stream().filter(a -> a.getName().equals(task)).findFirst().get();
        String annotation = activity.getAnnotation();
        ParticipantDTO[] participants = getAllParticipantsWithRolesAndDepartments(organizationalModel);
        final ParticipantDTO[] allParticipants = Arrays.copyOf(participants, participants.length);
        if(annotation == null)
        	return Arrays.stream(participants).map(participant -> participant.getBpmId()).toArray(String[]::new);
        //not using line splitter here because this string comes from camunda modeler, not from the frontend
        String[] lines = annotation.split("\n");
        for(String line : lines) {
	        String pattern = line.substring(0, line.indexOf("("));
	        String argument = line.substring(line.indexOf("(")+1, line.indexOf(")"));
	        switch(pattern) {
	        	case "role":
	        		participants = Arrays.stream(participants).filter(participant -> Arrays.stream(participant.getRoles().split("\\|")).anyMatch(argument::equals)).toArray(ParticipantDTO[]::new);
	        		break;
	        	case "department":
	        		participants = Arrays.stream(participants).filter(participant -> Arrays.stream(participant.getDepartments().split("\\|")).anyMatch(argument::equals)).toArray(ParticipantDTO[]::new);
	        		break;
	        	case "history":
	        		String historicalTask = argument.split(",")[0];
	        		String organizationalEntity = argument.split(",")[1];
	                Optional<Event> optionalEvent = log.getEvents().stream().filter(e -> e.getActivity().equals(historicalTask)).findFirst();
	                if(optionalEvent.isEmpty())
	                	//the task has not been executed yet -> ignore history constraint
	                	break;
	                Event historicalEvent = optionalEvent.get();
	                ParticipantDTO historicalResource = Arrays.stream(allParticipants).filter(participant -> participant.getBpmId().equals(historicalEvent.getResource())).findFirst().get();
	                switch(organizationalEntity) {
	                	case "resource":
	                		participants = Arrays.stream(participants).filter(participant -> participant.getBpmId().equals(historicalResource.getBpmId())).toArray(ParticipantDTO[]::new);
	                		break;
	                	case "role":
	                		String[] rolesOfHistoricalResource = historicalResource.getRoles().split("\\|");
	                		participants = Arrays.stream(participants).filter(participant -> haveItemInCommon(rolesOfHistoricalResource, participant.getRoles().split("\\|"))).toArray(ParticipantDTO[]::new);
	                		break;
	                	case "department":
	                		String[] departmentsOfHistoricalResource = historicalResource.getDepartments().split("\\|");
	                		participants = Arrays.stream(participants).filter(participant -> haveItemInCommon(departmentsOfHistoricalResource, participant.getDepartments().split("\\|"))).toArray(ParticipantDTO[]::new);
	                		break;
	                }
	                break;
	        	case "direct_allocation":
	        		participants = Arrays.stream(participants).filter(participant -> participant.getBpmId().equals(argument)).toArray(ParticipantDTO[]::new);
	        		break;
	        }
        }
        return Arrays.stream(participants).map(participant -> participant.getBpmId()).toArray(String[]::new);
    }
}
