package ubt.ai4.dpexdemo.multitenancy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import com.zaxxer.hikari.HikariDataSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

/**
 * Service responsible for executing SQL scripts to manage the database schemas for tenants.
 * It creates and deletes tenant-specific databases based on SQL scripts located in the application's
 * resources directory. This service is crucial for setting up new tenants and cleaning up resources
 * when tenants are removed.
 * <p>
 * The service is active only in profiles other than 'test' and 'dev', ensuring that these operations
 * are performed in production-like environments.
 */
@Service
@Profile({"!test & !dev"})
public class SQLExecutionService {

    @Value("${db.url.pattern}")
    private String dbUrlPattern;
    @Value("${db.username}")
    private String dbUsername;
    @Value("${db.password}")
    private String dbPassword;
    @Value("${db.driver}")
    private String dbDriver;

    private JdbcTemplate jdbcTemplate;

    /**
     * Initializes the service by creating a data source with administrative privileges.
     * This data source is used to execute SQL statements that manage the database schemas.
     */
    @PostConstruct
    @Profile({"!test & !dev"})
    private void init() {
        DataSource dataSource = createAdminDataSource();
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * Creates a {@link DataSource} with administrative privileges to the database. This data source
     * is used for executing SQL scripts that require elevated permissions, such as creating or
     * deleting databases.
     *
     * @return A {@link DataSource} configured with administrative credentials.
     */
    private DataSource createAdminDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(dbDriver);
        dataSource.setJdbcUrl(dbUrlPattern.replace("{tenantId}", "") + "?serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false");
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);

        return dataSource;
    }

    /**
     * Initializes the database schema for a new tenant using SQL scripts. This method reads
     * SQL statements from a script file located in the application's resources and executes them
     * to set up the tenant's database schema.
     *
     * @param tenantId The identifier for the tenant whose database schema is being initialized.
     */
    public void initializeTenantDatabase(String tenantId) {
        // Create the tenant's database
        jdbcTemplate.execute("CREATE DATABASE IF NOT EXISTS `" + tenantId + "`");
        jdbcTemplate.execute("USE `" + tenantId + "`"); // Switch to the tenant's database

        // Read and execute SQL statements from the provided sql-script in the applications resources
        try {
            Resource resource = new ClassPathResource("sql-scripts/initialize_tenant.sql");
            String sql = new BufferedReader(
                    new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n"));

            String[] sqlStatements = sql.split(";");

            for (String statement : sqlStatements) {
                if (!statement.trim().isEmpty()) {
                    jdbcTemplate.execute(statement);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes the database schema for a given tenant. This method executes a SQL statement to
     * drop the tenant's database, cleaning up all associated data and schema objects.
     *
     * @param tenantId The identifier for the tenant whose database schema is to be deleted.
     */
    public void deleteTenantDatabase(String tenantId) {
        String sqlDropDb = "DROP DATABASE IF EXISTS `" + tenantId + "`";
        jdbcTemplate.execute(sqlDropDb);
    }
}


