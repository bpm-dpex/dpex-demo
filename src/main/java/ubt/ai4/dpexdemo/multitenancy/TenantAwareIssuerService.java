package ubt.ai4.dpexdemo.multitenancy;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dpex.db.TenantContext;
import dpex.util.EncryptionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ubt.ai4.dpexdemo.db.EncryptedSecrets;
import ubt.ai4.dpexdemo.db.EncryptedSecretsRepository;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This service enables the application to adjust OAuth security and JWT decoding parameters dynamically based on the
 * current tenant context, ensuring that each tenant's data and access controls are properly isolated and managed.
 * This way requests from users of different realms can be validated dynamically in Keycloak. Additionally, the service
 * encrypts and decrypts the realm specific client secrets before it stores them in the database to enhance security.
 * These client secrets will later be used to perform realm specific calls to the keycloak Admin API for a realm admin.
 * <p>
 * This service is only active in profiles other than 'test' and 'dev', aligning with its usage in
 * production-like environments where multi-tenancy is a concern.
 */
@Service
public class TenantAwareIssuerService {
    private static final Logger logger = LoggerFactory.getLogger(TenantAwareIssuerService.class);
    private Map<String, String> issuerUriMap = new HashMap<>();
    private Map<String, String> jwkSetUriMap = new HashMap<>();
    private Map<String, String> clientSecretMap = new HashMap<>();

    @Value("${tenant.base-url}")
    private String baseUrl;

    public TenantAwareIssuerService() {
    }

    // Save clientSecrets in a secure manner --------------------------------------------------------------------------

    @Value("${app.encryption-key}")
    private String encryptionKey;

//    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
//    private static final String CHARSET_NAME = "UTF-8";
//    private static final String KEY_SPEC_ALGORITHM = "AES";

    private static EncryptionUtil encTool;


    /**
     * Initializes the service, loading any existing client secrets from the database and setting
     * up a default configuration for the master tenant. This method ensures that tenant-specific
     * settings are available upon application startup.
     */
    @PostConstruct
    @Profile({"!test & !dev"})
    void initialize() {
        encTool = new EncryptionUtil(encryptionKey);
        addTenantConfigurationNoSecretAtStartup("master");
        loadClientSecrets();
    }

    @Autowired
    private EncryptedSecretsRepository encryptedSecretsRepository;

    /**
     * Loads client secrets from the database, decrypting them for use in configuring tenant-specific
     * OAuth parameters. This method ensures that sensitive information is securely handled and stored.
     */
    private void loadClientSecrets() {
        try {
            TenantContext.clear(); //Should go in the default database
            Optional<EncryptedSecrets> optionalSecrets = encryptedSecretsRepository.findById(1L);
            if (optionalSecrets.isPresent()) {
                String encryptedSecrets = optionalSecrets.get().getSecrets();
                if (encryptedSecrets != null && !encryptedSecrets.isEmpty()) {
                    String decryptedContent = encTool.decrypt(encryptedSecrets);
                    ObjectMapper objectMapper = new ObjectMapper();
                    clientSecretMap = objectMapper.readValue(decryptedContent, new TypeReference<Map<String, String>>() {
                    });
                } else {
                    logger.info("No client secrets found in the database for the given ID.");
                }
            } else {
                logger.info("No entry found in the database for encrypted secrets.");
            }
        } catch (Exception e) {
            logger.error("An error occurred while loading client secrets from the database.", e);
        }
    }

    /**
     * Saves updated client secrets back to the database, encrypting them to ensure security.
     */
    public void saveClientSecrets() {
        try {
            TenantContext.clear(); //Should go in the default database
            ObjectMapper objectMapper = new ObjectMapper();
            String content = objectMapper.writeValueAsString(clientSecretMap);
            String encryptedContent = encTool.encrypt(content);

            EncryptedSecrets encryptedSecrets = new EncryptedSecrets();
            encryptedSecrets.setId(1L);
            encryptedSecrets.setSecrets(encryptedContent);
            encryptedSecretsRepository.save(encryptedSecrets);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    /**
//     * Encrypts the given input string using AES encryption with CBC mode and PKCS5 padding.
//     * This method ensures that sensitive information such as client secrets are securely encrypted
//     * before being stored in the database.
//     *
//     * @param input The plaintext string to be encrypted.
//     * @return A Base64 encoded string representing the encrypted version of the input.
//     * @throws Exception If any error occurs during the encryption process.
//     */
//    private String encrypt(String input) throws Exception {
//        Cipher cipher = Cipher.getInstance(ALGORITHM);
//        SecretKeySpec keySpec = new SecretKeySpec(encryptionKey.getBytes(CHARSET_NAME), KEY_SPEC_ALGORITHM);
//        IvParameterSpec ivParameterSpec = new IvParameterSpec(new byte[16]);
//        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivParameterSpec);
//        byte[] encrypted = cipher.doFinal(input.getBytes());
//        return Base64.getEncoder().encodeToString(encrypted);
//    }
//
//    /**
//     * Decrypts a given input string (which is Base64 encoded and encrypted) back into its plaintext form
//     * using AES decryption with CBC mode and PKCS5 padding.
//     *
//     * @param input The Base64 encoded string to be decrypted.
//     * @return The plaintext version of the input string.
//     * @throws Exception If any error occurs during the decryption process.
//     */
//    private String decrypt(String input) throws Exception {
//        Cipher cipher = Cipher.getInstance(ALGORITHM);
//        SecretKeySpec keySpec = new SecretKeySpec(encryptionKey.getBytes(CHARSET_NAME), KEY_SPEC_ALGORITHM);
//        IvParameterSpec ivParameterSpec = new IvParameterSpec(new byte[16]);
//        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivParameterSpec);
//        byte[] original = cipher.doFinal(Base64.getDecoder().decode(input));
//        return new String(original);
//    }

    // Save clientSecrets in a secure manner END -----------------------------------------------------------------------


    /**
     * Retrieves the issuer URI for a given tenant.
     *
     * @param tenantId The identifier of the tenant whose issuer URI is requested.
     * @return The issuer URI for the specified tenant.
     */
    public String getIssuerUri(String tenantId) {
        return issuerUriMap.get(tenantId);
    }

    /**
     * Retrieves the JWK Set URI for a given tenant.
     *
     * @param tenantId The identifier of the tenant whose JWK Set URI is requested.
     * @return The JWK Set URI for the specified tenant.
     */
    public String getJwkSetUri(String tenantId) {
        return jwkSetUriMap.get(tenantId);
    }

    /**
     * Retrieves the client secret for a given tenant.
     *
     * @param tenantId The identifier of the tenant whose client secret is requested.
     * @return The decrypted client secret for the specified tenant.
     */
    public String getClientSecret(String tenantId) {
        return clientSecretMap.get(tenantId);
    }

    /**
     * Adds or updates configuration for a tenant, including issuer URI, JWK Set URI, and encrypted
     * client secret.
     *
     * @param tenantId     The identifier of the tenant for whom the configuration is being added or updated.
     * @param clientSecret The client secret associated with the tenant, to be encrypted and stored.
     */
    public void addTenantConfiguration(String tenantId, String clientSecret) {
        String issuerUri = baseUrl + tenantId;
        String jwkSetUri = issuerUri + "/protocol/openid-connect/certs";

        issuerUriMap.put(tenantId, issuerUri);
        jwkSetUriMap.put(tenantId, jwkSetUri);
        clientSecretMap.put(tenantId, clientSecret);
        saveClientSecrets();
    }

    /**
     * Removes configuration for a tenant, cleaning up any stored issuer URI, JWK Set URI, and client
     * secret.
     *
     * @param tenantId The identifier of the tenant whose configuration is to be removed.
     */
    public void removeTenantConfiguration(String tenantId) {
        issuerUriMap.remove(tenantId);
        jwkSetUriMap.remove(tenantId);
        clientSecretMap.remove(tenantId);
        saveClientSecrets();
    }

    /**
     * Adds configuration for a tenant at application startup without requiring a client secret.
     * This method is used to pre-configure issuer URI and JWK Set URI for tenants for whom
     * a client secret is not yet available or necessary.
     *
     * @param tenantId The identifier of the tenant for whom the configuration is being added.
     */
    public void addTenantConfigurationNoSecretAtStartup(String tenantId) {
        String issuerUri = baseUrl + tenantId;
        String jwkSetUri = issuerUri + "/protocol/openid-connect/certs";
        issuerUriMap.put(tenantId, issuerUri);
        jwkSetUriMap.put(tenantId, jwkSetUri);
    }


}
