package ubt.ai4.dpexdemo.multitenancy;

import dpex.db.AllianceRepository;
import dpex.db.TenantContext;
import dpex.impl.collaboration.bftsmart.BftSmartNetwork;
import dpex.impl.collaboration.ethereum.raw.EthereumRawNetwork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ubt.ai4.dpexdemo.api.KeycloakAdminService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.Timer;


/**
 * Service responsible for dynamically managing the tenants. This includes
 * registering new tenants with their own databases, OAuth configurations, and Keycloak realms, as
 * well as deleting tenants and their associated resources. The service integrates multiple components
 * to handle data source routing, tenant-specific OAuth settings, SQL schema initialization, and
 * Keycloak realm management.
 * <p>
 * This service is only active in production where multi tenancy is activated.
 */
@Service
@Profile({"!test & !dev"})
public class TenantService {

    private final MultiTenancyConfig multiTenancyConfig;
    private final TenantAwareIssuerService tenantAwareIssuerService;
    private final SQLExecutionService sqlExecutionService;
    private final KeycloakAdminService keycloakAdminService;
    private final AllianceRepository allianceRepository;

    /**
     * Constructs the TenantService with dependencies on other services required for
     * tenant management, including data source configuration, OAuth settings, SQL execution,
     * and Keycloak administration.
     *
     * @param multiTenancyConfig       The configuration service for multi-tenancy data source routing.
     * @param tenantAwareIssuerService The service for managing tenant-specific OAuth settings and client secrets.
     * @param sqlExecutionService      The service for executing SQL scripts for tenant database initialization.
     * @param keycloakAdminService     The service for managing Keycloak realms and users.
     */
    @Autowired
    public TenantService(MultiTenancyConfig multiTenancyConfig, TenantAwareIssuerService tenantAwareIssuerService,
                         SQLExecutionService sqlExecutionService, @Lazy KeycloakAdminService keycloakAdminService, AllianceRepository allianceRepository) {
        this.multiTenancyConfig = multiTenancyConfig;
        this.tenantAwareIssuerService = tenantAwareIssuerService;
        this.sqlExecutionService = sqlExecutionService;
        this.keycloakAdminService = keycloakAdminService;
        this.allianceRepository = allianceRepository;
    }

    /**
     * Registers a new tenant in the system. This process includes creating a new tenant-specific
     * database, setting up OAuth configurations and the client secret, initializing the SQL schema, and creating a
     * corresponding realm in Keycloak with a default user.
     *
     * @param tenantId       The unique identifier for the new tenant.
     * @param clientSecret   The client secret for the tenant's admin API.
     * @param username       The username for the default tenant admin user.
     * @param email          The email for the default tenant admin user.
     * @param password       The password for the default tenant admin user.
     * @param firstName      The first name for the default tenant admin user.
     * @param lastName       The last name for the default tenant admin user.
     * @param accountDetails Additional account details for the default tenant admin user.
     */
    public void registerTenant(String tenantId, String clientSecret, String username, String email, String password, String firstName, String lastName, String accountDetails) {
        multiTenancyConfig.addTenantDataSource(tenantId);
        tenantAwareIssuerService.addTenantConfiguration(tenantId, clientSecret);
        sqlExecutionService.initializeTenantDatabase(tenantId);

        // Create a realm in Keycloak for the new tenant
        try {
            keycloakAdminService.createRealmFromStockTemplate(tenantId, clientSecret);
            keycloakAdminService.createUser(tenantId, username, email, password, firstName, lastName, accountDetails);
        } catch (Exception e) {
            throw new RuntimeException("Error creating Keycloak realm for tenant: " + tenantId, e);
        }
    }

    /**
     * Deletes a tenant from the system. This process involves removing the tenant's listeners/timers, database,
     * OAuth configurations and client secret, and the corresponding Keycloak realm.
     *
     * @param tenantId The unique identifier for the tenant to be deleted.
     */
    public void deleteTenant(String tenantId) {
        TenantContext.setTenantId(tenantId);
        allianceRepository.findAll().forEach(alliance -> {
            if (alliance.getCollaboration().getNetwork() instanceof BftSmartNetwork) {
                BftSmartNetwork bftnetwork = (BftSmartNetwork) alliance.getCollaboration().getNetwork();
                Timer timer = BftSmartNetwork.polling_timers.get(tenantId + bftnetwork.networkString);
                if (timer != null) {
                    timer.cancel();
                }
            }
            if (alliance.getCollaboration().getNetwork() instanceof EthereumRawNetwork) {
                EthereumRawNetwork ethnetwork = (EthereumRawNetwork) alliance.getCollaboration().getNetwork();
                ethnetwork.disposeListerners();
            }
        });
        TenantContext.clear();
        sqlExecutionService.deleteTenantDatabase(tenantId);
        multiTenancyConfig.removeTenantDataSource(tenantId);
        // For some reason even though Tenant Context is cleared, the Data Source Routing won't reset, this is a work-
        // around to allow this expression to target the default datasource and not the one used to query alliances
        new java.util.Timer().schedule(new java.util.TimerTask() { public void run() { tenantAwareIssuerService.removeTenantConfiguration(tenantId); this.cancel(); } }, 0);
        // Delete the realm in Keycloak for the tenant
        keycloakAdminService.deleteRealm(tenantId);
    }

    /**
     * At application startup, registers existing realms from Keycloak as tenants in the
     * application. This ensures that the application's tenant configurations are synchronized
     * with the current state of Keycloak realms. In case of failure to fetch realms from Keycloak,
     * attempts to terminate the application process to prevent an inconsistent state.
     */
    @PostConstruct
    public void registerExistingRealmsAsTenants() {
        try {
            List<String> existingRealms = keycloakAdminService.getAllRealms();
            existingRealms.forEach(realmName -> {
                tenantAwareIssuerService.addTenantConfigurationNoSecretAtStartup(realmName);
                multiTenancyConfig.addTenantDataSource(realmName);
            });
        } catch (Exception e) {
            System.err.println("Critical error during startup: Failed to fetch realms from Keycloak.");

            String pid = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
            try {
                // Execute system command to kill the process, only option possible here since application context not yet available in Springboot on Bean Creation and System.exit() does not kill the application completely
                Runtime.getRuntime().exec("kill " + pid);
            } catch (IOException re) {
                re.printStackTrace();
            }

        }
    }
}
