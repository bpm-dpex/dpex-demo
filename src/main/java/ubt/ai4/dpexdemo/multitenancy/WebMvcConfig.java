package ubt.ai4.dpexdemo.multitenancy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Web MVC configuration class responsible for registering the {@link TenantCleanupInterceptor}
 * with the Spring MVC interceptor registry. This configuration ensures that the tenant context
 * is properly cleared at the end of processing each HTTP request, maintaining clean state management
 * and preventing potential cross-tenant data leaks in a multi-tenant environment.
 *
 * This configuration is only active in production where multi tenancy is activated.
 */
//@Profile({ "!test & !dev" })
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private final TenantCleanupInterceptor tenantCleanupInterceptor;

    /**
     * Constructs the WebMvcConfig with the {@link TenantCleanupInterceptor}.
     *
     * @param tenantCleanupInterceptor The interceptor responsible for clearing the tenant context after each request.
     */
    @Autowired
    public WebMvcConfig(TenantCleanupInterceptor tenantCleanupInterceptor) {
        this.tenantCleanupInterceptor = tenantCleanupInterceptor;
    }

    /**
     * Adds the {@link TenantCleanupInterceptor} to the application's interceptor registry.
     * By registering the interceptor, it is ensured that the interceptor's afterCompletion method
     * is invoked after each request, thereby clearing the tenant context from the thread-local storage.
     *
     * @param registry The interceptor registry where the interceptor is registered.
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(tenantCleanupInterceptor);
    }
}

