package ubt.ai4.dpexdemo.multitenancy;

import dpex.db.TenantContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * Configuration class to support multi-tenancy in the application by dynamically routing to
 * different data sources based on the tenant context. This class allows for the separation
 * of tenant-specific data across in this app unique schemas.
 * <p>
 * The configuration is active only in profiles other than 'test' and 'dev', ensuring that
 * multi-tenancy data source routing is only enabled in production environments.
 */
@Profile({"!test & !dev"})
@Configuration
public class MultiTenancyConfig {

    private Map<Object, Object> targetDataSources = new HashMap<>();
    private AbstractRoutingDataSource multiTenantDataSource;

    @Value("${db.url.pattern}")
    private String dbUrlPattern;
    @Value("${db.username}")
    private String dbUsername;
    @Value("${db.password}")
    private String dbPassword;
    @Value("${db.driver}")
    private String dbDriver;
    @Value("${db.defaulttenant}")
    private String dbDefaultTenant;

    /**
     * Initializes the multi-tenant data source to use TenantContext from dpex-lib for routing.
     */
    @PostConstruct
    public void init() {
        multiTenantDataSource = new AbstractRoutingDataSource() {
            @Override
            protected Object determineCurrentLookupKey() {
                String tenantId = TenantContext.getTenantId();
                return tenantId;
            }
        };

        multiTenantDataSource.setTargetDataSources(targetDataSources);
        multiTenantDataSource.setDefaultTargetDataSource(defaultDataSource()); // Fallback data source
        multiTenantDataSource.afterPropertiesSet();
    }

    /**
     * Exposes the multi-tenant data source bean to the Spring context. This data source
     * routes database operations to the correct tenant-specific data source based on the
     * current tenant context.
     *
     * @return The configured multi-tenant data source.
     */
    @Bean
    public DataSource dataSource() {
        return multiTenantDataSource;
    }

    /**
     * Adds a new tenant-specific data source to the routing configuration. This method
     * dynamically configures access to a new tenant's database or schema based on provided
     * tenant identifier.
     *
     * @param tenantId The identifier for the new tenant.
     */
    public void addTenantDataSource(String tenantId) {
        String tenantUrl = dbUrlPattern.replace("{tenantId}", tenantId);
        DriverManagerDataSource dataSource = new DriverManagerDataSource(tenantUrl, dbUsername, dbPassword);
        dataSource.setDriverClassName(dbDriver);
        targetDataSources.put(tenantId, dataSource);
        multiTenantDataSource.setTargetDataSources(targetDataSources);
        multiTenantDataSource.afterPropertiesSet();
    }

    /**
     * Removes a tenant-specific data source from the routing configuration. This is useful
     * for cleaning up resources when a tenant is removed from the application.
     *
     * @param tenantId The identifier for the tenant whose data source should be removed.
     */
    public void removeTenantDataSource(String tenantId) {
        targetDataSources.remove(tenantId);
        multiTenantDataSource.setTargetDataSources(targetDataSources);
        multiTenantDataSource.afterPropertiesSet();
    }

    /**
     * Configures and returns the default data source, which is used as a fallback or for
     * system operations not specific to any tenant. This is used for the encrypted client secrets of the tenants
     * corresponding keycloak realms, as well as for the local Camunda engine.
     *
     * @return The default data source for the application.
     */
    public DataSource defaultDataSource() {
        // Configure the default data source, used by camunda
        DriverManagerDataSource d = new DriverManagerDataSource(dbUrlPattern.replace("{tenantId}", dbDefaultTenant), dbUsername, dbPassword);
        d.setDriverClassName(dbDriver);
        return d;
    }
}
