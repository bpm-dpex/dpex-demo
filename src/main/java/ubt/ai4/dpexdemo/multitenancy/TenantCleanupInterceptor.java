package ubt.ai4.dpexdemo.multitenancy;

import dpex.db.TenantContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * Interceptor for cleaning up the tenant context after a request has been processed.
 * This component ensures that the tenant context is cleared from the current thread
 * once the request handling is complete, preventing potential leaks of tenant-specific
 * information into subsequent requests handled by the same thread.
 */
@Component
public class TenantCleanupInterceptor implements HandlerInterceptor {

    @Autowired
    private Environment env;

    private boolean isDevProfileActive() {
        return Arrays.asList(env.getActiveProfiles()).contains("dev");
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (isDevProfileActive()) {
            TenantContext.setTenantId("dev");
        }
        // Nothing needed here for tenant cleanup, but required for interface compliance.
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // No tenant cleanup needed here, but method is required for interface compliance.
    }

    /**
     * Callback method for cleaning up resources after request completion.
     * This method clears the tenant context, ensuring the thread local used to store
     * the tenant id is reset for the next request handled by the same thread.
     *
     * @param request  The HTTP request.
     * @param response The HTTP response.
     * @param handler  The handler that was executed.
     * @param ex       Any exception thrown on handler execution, if any.
     * @throws Exception in case of errors during cleanup.
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        TenantContext.clear();
    }
}

