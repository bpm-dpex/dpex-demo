package ubt.ai4.dpexdemo.multitenancy;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import dpex.db.TenantContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A filter that precedes Spring Security's authentication process to manually parse
 * JWT tokens for the tenant ID. This is necessary because the authentication process
 * relies on the tenant ID, which doubles as the realm name in Keycloak for multi-tenancy
 * support. The filter ensures that the tenant context is set early in the request processing
 * lifecycle based on the tenant ID extracted from the JWT.
 * <p>
 * This filter is active only in production where multi tenancy is activated.
 */
@Profile({"!test & !dev"})
@Component
public class TenantIdentificationFilter extends GenericFilterBean {

    /**
     * Processes each incoming HTTP request, extracting the JWT token to set the tenant context
     * before any Spring Security authentication takes place. It bypasses tenant identification
     * for requests targeting specific endpoints (e.g., Camunda engine endpoints) that do not
     * support multi-tenancy.
     *
     * @param request  The servlet request.
     * @param response The servlet response.
     * @param chain    The filter chain.
     * @throws IOException      If an input or output exception occurs.
     * @throws ServletException If the request could not be handled.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        // Check if the request is targeting Camunda endpoints, here the default datasource should be used
        String path = httpServletRequest.getRequestURI();
        if (path.startsWith("/engine-rest")) {
            TenantContext.clear();
            // Skip tenant identification for Camunda requests
            chain.doFilter(request, response);
            return;
        }

        String jwtToken = extractJwtFromRequest(httpServletRequest);
        if (jwtToken != null && !jwtToken.isBlank()) {
            String tenantId = extractTenantIdFromJwt(jwtToken);
            TenantContext.setTenantId(tenantId);
        }
        chain.doFilter(request, response);
    }

    /**
     * Attempts to extract the JWT token from the incoming request. It first looks for the token
     * in the Authorization header as a Bearer token. Failing that, it tries to extract the token
     * from the request's query parameters (WebSocket Handshake Requests need this).
     *
     * @param request The HTTP servlet request.
     * @return The extracted JWT token if present; null otherwise.
     */
    private String extractJwtFromRequest(HttpServletRequest request) {
        // First, try to get the token from the Authorization header
        String bearerToken = request.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        } else {
            // If not found in the Authorization header, try to get it from query parameters
            String queryString = request.getQueryString();
            if (queryString != null) {
                Map<String, String> queryParamMap = Arrays.stream(queryString.split("&"))
                        .map(param -> param.split("="))
                        .collect(Collectors.toMap(param -> URLDecoder.decode(param[0], StandardCharsets.UTF_8), param -> URLDecoder.decode(param[1], StandardCharsets.UTF_8)));
                return queryParamMap.getOrDefault("token", null);
            }
        }
        return null;
    }

    /**
     * Extracts the tenant ID from the decoded JWT token.
     *
     * @param jwtToken The JWT token from which to extract the tenant ID.
     * @return The extracted tenant ID, or a default value if extraction fails.
     */
    private String extractTenantIdFromJwt(String jwtToken) {
        try {
            DecodedJWT jwt = JWT.decode(jwtToken);
            String issuerUrl = jwt.getIssuer();
            String tenantId = issuerUrl.substring(issuerUrl.lastIndexOf('/') + 1);
            return tenantId;
        } catch (Exception e) {
            logger.error("Error extracting tenant ID from JWT", e);
            return "default";
        }
    }
}
