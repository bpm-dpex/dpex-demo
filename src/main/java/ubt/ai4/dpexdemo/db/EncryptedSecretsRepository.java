package ubt.ai4.dpexdemo.db;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link EncryptedSecrets} entities.
 */
public interface EncryptedSecretsRepository extends JpaRepository<EncryptedSecrets, Long> {
}

