package ubt.ai4.dpexdemo.db;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link EncryptedEthereumKey} entities.
 */
public interface EncryptedEthereumKeyRepository  extends JpaRepository<EncryptedEthereumKey, String> {
}
