package ubt.ai4.dpexdemo.db;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Entity class representing encrypted client secrets for the Keycloak Admin API for the current tenant stored in the database.
 * This class is used to safely store and retrieve encrypted secrets associated with
 * dynamically added or deleted tenants.
 */
@Entity
@Table(name = "encrypted_secrets")
public class EncryptedSecrets {

    @Id
    private Long id;

    @Lob
    private String secrets;

    public EncryptedSecrets() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSecrets() {
        return secrets;
    }

    public void setSecrets(String secrets) {
        this.secrets = secrets;
    }
}
