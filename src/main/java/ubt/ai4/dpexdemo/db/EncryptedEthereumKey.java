package ubt.ai4.dpexdemo.db;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.IOException;
import java.util.TreeMap;

/**
 * Entity class for securely storing encrypted Ethereum private keys for a user in the MySQL Database.
 * This class is mapped to the "encrypted_ethereum_keys" table. It handles the serialization
 * and deserialization of user-specific Ethereum keys into and from JSON format, the ethereum private keys are stored
 * in a map, with the BPM ID being the key.
 */
@Entity
@Table(name = "encrypted_ethereum_keys")
public class EncryptedEthereumKey {

    @Id
    private String username;

    private String secrets;

    /**
     * Default constructor for JPA use only.
     */
    public EncryptedEthereumKey() {
    }

    /**
     * Retrieves the username associated with this entity.
     *
     * @return The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username associated with the encrypted Ethereum keys.
     *
     * @param username The username to set.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Retrieves the JSON string that contains encrypted Ethereum private keys.
     *
     * @return The secrets as a JSON string.
     */
    public String getSecrets() {
        return secrets;
    }

    /**
     * Sets the encrypted Ethereum private keys in a JSON string format.
     *
     * @param secrets The JSON string containing the encrypted Ethereum private keys.
     */
    public void setSecrets(String secrets) {
        this.secrets = secrets;
    }

    /**
     * Deserializes the JSON-formatted secrets string into a TreeMap, for access to individual encrypted keys.
     *
     * @return A TreeMap<String, String> parsed from the encrypted secrets JSON, or an empty map if the secrets string is null.
     * @throws IOException If there is an issue parsing the JSON string into a TreeMap.
     */
    public TreeMap<String, String> getSecretsMap() throws IOException {
        if (secrets == null) {
            return new TreeMap<>();
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(secrets, new TypeReference<TreeMap<String, String>>() {
        });
    }

    /**
     * Serializes a TreeMap containing encrypted Ethereum private keys into a JSON string and sets this string in the 'secrets' field.
     *
     * @param secretsMap The TreeMap<String, String> of encrypted keys to serialize.
     * @throws IOException If there is an issue converting the TreeMap into a JSON string.
     */
    public void setSecretsMap(TreeMap<String, String> secretsMap) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        this.secrets = mapper.writeValueAsString(secretsMap);
    }
}
