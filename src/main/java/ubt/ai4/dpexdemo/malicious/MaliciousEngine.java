package ubt.ai4.dpexdemo.malicious;

import dpex.action.TaskAction;
import dpex.bpm.execution.Instance;
import dpex.bpm.model.ProcessModel;
import dpex.impl.bpm.camunda.CamundaExecutionEngine;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class MaliciousEngine extends CamundaExecutionEngine {


    @Override
    public boolean isConform(ProcessModel processModel, Instance instance, TaskAction taskAction) {
        return true;
    }

}
