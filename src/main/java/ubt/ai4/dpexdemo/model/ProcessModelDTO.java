package ubt.ai4.dpexdemo.model;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public
class ProcessModelDTO {

    private MultipartFile processModel;
    private String organizationalModel;
}
