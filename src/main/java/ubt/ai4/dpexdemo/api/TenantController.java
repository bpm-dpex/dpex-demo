package ubt.ai4.dpexdemo.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ubt.ai4.dpexdemo.multitenancy.TenantService;

import java.util.ArrayList;
import java.util.List;

/**
 * The TenantController class provides RESTful web services for managing tenants in the application.
 * It offers endpoints for registering new tenants, deleting existing tenants, and retrieving all tenant/realm names.
 * Each tenant can be mapped to exactly one realm in Keycloak, so this controller interacts with KeycloakAdminService
 * to manage tenant realms as well.
 */
@CrossOrigin
@RestController
@Profile({"!test & !dev"})
@RequestMapping("/api/tenants")
public class TenantController {

    private final TenantService tenantService;
    @Autowired
    private KeycloakAdminService keycloakAdminService;


    /**
     * Constructs the TenantController with the necessary tenant service dependency.
     *
     * @param tenantService The service responsible for managing tenants - including database and keycloak interactions.
     */
    public TenantController(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    /**
     * Endpoint for registering a new tenant. This method requires admin (of the master realm) privileges and utilizes
     * the TenantService to register a new tenant in the application and Keycloak.
     *
     * @param request The tenant registration request containing necessary details like tenant ID, client secret, and user information for the first user.
     * @return A ResponseEntity containing a success message.
     */
    @PostMapping("/register")
    @PreAuthorize("@securityUtils.checkIfUserIsGlobalAdminInSaaSSystem()")
    public ResponseEntity<String> registerTenant(@RequestBody TenantRegistrationRequestDTO request) {
        tenantService.registerTenant(request.getTenantId(), request.getClientSecret(), request.getUsername(), request.getEmail(), request.getPassword(), request.getFirstName(), request.getLastName(), request.getAccountDetails());
        return ResponseEntity.ok("Tenant registered successfully.");
    }


    /**
     * Endpoint for deleting an existing tenant. This method also requires admin privileges (of the master realm)  and
     * delegates to the TenantService to remove a tenant from the application and Keycloak.
     *
     * @param tenantId The ID of the tenant to be deleted.
     * @return A ResponseEntity containing a success message.
     */
    @DeleteMapping("/delete/{tenantId}")
    @PreAuthorize("@securityUtils.checkIfUserIsGlobalAdminInSaaSSystem()")
    public ResponseEntity<String> deleteTenant(@PathVariable String tenantId) {
        tenantService.deleteTenant(tenantId);
        return ResponseEntity.ok("Tenant deleted successfully.");
    }

    /**
     * Endpoint to retrieve all existing tenant's names. This method is publicly accessible and
     * returns a list of all tenant names managed by the application.
     *
     * @return A ResponseEntity with a list of all tenant names.
     */
    @GetMapping("/all-tenants")
    public ResponseEntity<List<String>> getAllTenants() {
        List<String> allRealmNames = keycloakAdminService.getAllRealms();
        if (allRealmNames == null) {
            allRealmNames = new ArrayList<>();
        }
        return ResponseEntity.ok(allRealmNames);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class TenantRegistrationRequestDTO {
        private String tenantId;
        private String clientSecret;
        private String username;
        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private String accountDetails;
    }
}

