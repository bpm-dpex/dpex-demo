package ubt.ai4.dpexdemo.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dpex.core.User;
import dpex.db.TenantContext;
import jakarta.ws.rs.core.Response;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import ubt.ai4.dpexdemo.multitenancy.TenantAwareIssuerService;


import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static ubt.ai4.dpexdemo.api.UserManagementController.extractBpmIds;
import static ubt.ai4.dpexdemo.utils.KeycloakUtils.isUuid;
import static ubt.ai4.dpexdemo.utils.KeycloakUtils.loadJsonTemplate;

/**
 * The KeycloakAdminService class provides functionalities to manage Keycloak realms, users, and roles.
 * It offers a way to connect to a Keycloak server and perform various operations such as creating users,
 * managing realms, and assigning roles, leveraging the Keycloak Admin CLI
 * This service uses a global client secret for operations requiring master realm access, while
 * also enabling DPEX administrators of local tenants to manage
 * user rights and account details.
 */
@Service
@Lazy
public class KeycloakAdminService {

    @Value("${keycloak-admin.client-id}")
    private String clientId;
    @Value("${keycloak-admin.auth-server-url}")
    private String serverUrl;
    @Value("${keycloak-admin.managed-client-id}")
    private String managedClientId;

    @Value("${tenant.master-realm-name}")
    private String masterRealmName;
    @Value("${tenant.master-realm-client-name}")
    private String masterRealmClientName;
    @Value("${tenant.master-realm-client-secret}")
    private String masterRealmClientSecret;

    @Autowired
    private TenantAwareIssuerService tenantAwareIssuerService;

    @Autowired
    private ResourceLoader resourceLoader;

    /**
     * Gets a Keycloak client for the current tenant. This method dynamically identifies the tenant from
     * the TenantContext and uses the tenant-specific client secret for authentication.
     *
     * @return A Keycloak client configured for the current tenant.
     */
    private Keycloak getKeycloakClient() {
        String realm = TenantContext.getTenantId();
        String clientSecret = tenantAwareIssuerService.getClientSecret(realm);

        return KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(realm)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .grantType("client_credentials")
                .build();
    }

    /**
     * Creates a Keycloak instance for the master realm using the service's configured properties.
     *
     * @return A Keycloak client connected to the master realm.
     */
    private Keycloak masterRealmKeycloakClient() {
        return KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(masterRealmName)
                .clientId(masterRealmClientName)
                .clientSecret(masterRealmClientSecret)
                .grantType("client_credentials")
                .build();
    }

    /**
     * Creates a new user in the specified realm with the given details.
     * This method sets up a new user representation, assigns it a password, and assigns an admin role
     * within the specified realm.
     *
     * @param realmName      The name of the realm where the user will be created.
     * @param username       The username for the new user.
     * @param email          The email address for the new user.
     * @param password       The password for the new user.
     * @param firstName      The first name of the new user.
     * @param lastName       The last name of the new user.
     * @param accountDetails Additional account details to be associated with the new user (e.g. BPM IDs).
     */
    public void createUser(String realmName, String username, String email, String password, String firstName, String lastName, String accountDetails) {
        Keycloak keycloak = masterRealmKeycloakClient();

        // Initialize user representation
        UserRepresentation newUser = new UserRepresentation();
        newUser.setUsername(username);
        newUser.setEmail(email);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEnabled(true);

        // Set custom attribute
        Map<String, List<String>> attributes = new HashMap<>();
        attributes.put("accountDetails", Collections.singletonList(accountDetails));
        newUser.setAttributes(attributes);

        // Set credentials (password) for the user
        CredentialRepresentation passwordCreds = new CredentialRepresentation();
        passwordCreds.setType(CredentialRepresentation.PASSWORD);
        passwordCreds.setValue(password);
        passwordCreds.setTemporary(false);
        newUser.setCredentials(Collections.singletonList(passwordCreds));

        // Get realm resource and create the user
        RealmResource targetRealm = keycloak.realm(realmName);
        Response response = targetRealm.users().create(newUser);

        String userId = CreatedResponseUtil.getCreatedId(response);

        // Assign admin role in dpex-frontend client to the user
        assignAdminRole(targetRealm, userId);

        if (!keycloak.isClosed()) {
            keycloak.close();
        }
    }

    /**
     * Assigns the admin role to a user within a specified realm. This involves fetching the client
     * resources for the realm, identifying the "dpex-frontend" client, and assigning the "admin" role
     * from it to the user. Additionally, assigns the "realm-admin" role from the "realm-management" client.
     *
     * @param targetRealm The realm resource in which the user resides.
     * @param userId      The ID of the user to whom the admin role will be assigned.
     */
    private void assignAdminRole(RealmResource targetRealm, String userId) {
        ClientsResource clientsResource = targetRealm.clients();
        List<ClientRepresentation> clients = clientsResource.findAll();
        ClientRepresentation dpexFrontendClient = clients.stream()
                .filter(client -> managedClientId.equals(client.getClientId()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("managed client id not found"));

        RoleRepresentation adminRole = clientsResource.get(dpexFrontendClient.getId()).roles().get("admin").toRepresentation();
        targetRealm.users().get(userId).roles().clientLevel(dpexFrontendClient.getId()).add(Collections.singletonList(adminRole));

        ClientRepresentation realmManagementClient = clients.stream()
                .filter(client -> "realm-management".equals(client.getClientId()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Realm-management client not found"));

        RoleRepresentation realmAdminRole = clientsResource.get(realmManagementClient.getId()).roles().get("realm-admin").toRepresentation();
        targetRealm.users().get(userId).roles().clientLevel(realmManagementClient.getId()).add(Collections.singletonList(realmAdminRole));
    }


    /**
     * Retrieves all realm names excluding the master realm.
     *
     * @return A list of names of all realms, excluding the master realm.
     */
    public List<String> getAllRealms() {
        Keycloak keycloak = masterRealmKeycloakClient();
        // Obtain all realms
        List<RealmRepresentation> realms = keycloak.realms().findAll();
        if (!keycloak.isClosed()) {
            keycloak.close();
        }
        // Filter out the master realm and collect the names of the remaining realms
        return realms.stream()
                .filter(realm -> !realm.getRealm().equals(masterRealmName))
                .map(RealmRepresentation::getRealm)
                .collect(Collectors.toList());
    }

    /**
     * Creates a new realm based on a stock template json from the resources directory and the provided realm name
     * and client secret.
     * The method modifies the stock template's UUIDs and realm name to create a new realm configuration.
     *
     * @param realmName    The name of the new realm to be created.
     * @param clientSecret The client secret to be used for the new realm's dpex-backend client.
     * @throws IOException If there's an issue loading the JSON template or modifying the realm.
     */
    public void createRealmFromStockTemplate(String realmName, String clientSecret) throws IOException {
        String jsonTemplate = loadJsonTemplate(resourceLoader, "dpex-realm-export.json");
        try {
            modifyAndCreateRealm(jsonTemplate, realmName, clientSecret);
        } catch (Exception e) {
            throw new IOException("Failed to modify or create realm", e);
        }
    }

    /**
     * Modifies and creates a new realm based on a JSON template. This method performs modifications on the
     * template, such as updating UUIDs and realm names, before creating the new realm in Keycloak.
     *
     * @param jsonTemplate The JSON template as a String.
     * @param newRealmName The name for the new realm.
     * @param clientSecret The client secret to be used in the new realm.
     * @throws IOException If there's an issue modifying the template or creating the new realm.
     */
    private void modifyAndCreateRealm(String jsonTemplate, String newRealmName, String clientSecret) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(jsonTemplate);
        replaceUuidsAndRealmName(root, newRealmName);
        updateDpexBackendClientSecret(root, clientSecret);

        String modifiedJsonTemplate = mapper.writeValueAsString(root);
        RealmRepresentation newRealm = mapper.readValue(modifiedJsonTemplate, RealmRepresentation.class);

        Keycloak keycloak = masterRealmKeycloakClient();
        keycloak.realms().create(newRealm);
        if (!keycloak.isClosed()) {
            keycloak.close();
        }
    }

    /**
     * Updates the client secret for the "dpex-backend" client within a given JSON node representing a realm configuration.
     *
     * @param node         The JSON node containing the clients configuration.
     * @param clientSecret The new client secret to be set for the "dpex-backend" client.
     */
    private void updateDpexBackendClientSecret(JsonNode node, String clientSecret) {
        if (node.isObject()) {
            ObjectNode objectNode = (ObjectNode) node;
            JsonNode clientsNode = objectNode.get("clients");
            if (clientsNode != null && clientsNode.isArray()) {
                ArrayNode clientsArray = (ArrayNode) clientsNode;
                for (JsonNode clientNode : clientsArray) {
                    if (clientNode.has("clientId") && "dpex-backend".equals(clientNode.get("clientId").textValue())) {
                        ((ObjectNode) clientNode).put("secret", clientSecret);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Replaces UUIDs and the realm name within a JSON node. This method is used to prepare a realm configuration
     * template for the creation of a new realm by assigning new UUIDs and updating the realm name. Otherwise Keycloak
     * would reject the new realm with the error "realm already exists".
     *
     * @param node         The JSON node to be modified.
     * @param newRealmName The new realm name to replace the old one in the template.
     */
    private void replaceUuidsAndRealmName(JsonNode node, String newRealmName) {
        if (node.isObject()) {
            ObjectNode object = (ObjectNode) node;
            Iterator<Map.Entry<String, JsonNode>> fields = object.fields();
            while (fields.hasNext()) {
                Map.Entry<String, JsonNode> field = fields.next();
                if (field.getValue().isTextual()) {
                    String value = field.getValue().textValue();
                    if ("id".equals(field.getKey()) || isUuid(value)) {
                        object.put(field.getKey(), UUID.randomUUID().toString());
                    } else if (value.equals("dpex")) { //  || value.matches("some-regex-here") Just in case I want to later have a more granular approach
                        object.put(field.getKey(), value.replace("dpex", newRealmName));
                    }
                } else {
                    replaceUuidsAndRealmName(field.getValue(), newRealmName);
                }
            }
        } else if (node.isArray()) {
            for (JsonNode arrayItem : node) {
                replaceUuidsAndRealmName(arrayItem, newRealmName);
            }
        }
    }

    /**
     * Deletes a specified realm. This operation is irreversible and should be used with caution.
     *
     * @param realmName The name of the realm to be deleted.
     */
    public void deleteRealm(String realmName) {
        Keycloak keycloak = masterRealmKeycloakClient();
        keycloak.realm(realmName).remove();
        if (!keycloak.isClosed()) {
            keycloak.close();
        }
    }

    /**
     * Retrieves all users from the current tenant's realm. This method uses the tenant-aware Keycloak client
     * to fetch and return a list of all user representations.
     *
     * @return A list of UserRepresentation objects for all users in the current realm.
     */
    public List<UserRepresentation> getUsers() {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId();
        List<UserRepresentation> users = keycloak.realm(realm).users().list();
        if (!keycloak.isClosed()) {
            keycloak.close();
        }
        return users;
    }

    /**
     * Retrieves all realm-level roles assigned to a specific user within the current tenant's realm. This method
     * fetches and returns a list of all role names assigned to the user.
     *
     * @param userId The ID of the user whose realm-level roles are to be retrieved.
     * @return A list of names of the realm-level roles assigned to the user.
     */
    public List<String> getUserRealmRoles(String userId) {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId();
        UsersResource usersResource = keycloak.realm(realm).users();
        UserResource userResource = usersResource.get(userId);
        List<String> roles = userResource.roles().realmLevel().listAll().stream()
                .map(RoleRepresentation::getName)
                .collect(Collectors.toList());
        if (!keycloak.isClosed()) {
            keycloak.close();
        }
        return roles;
    }

    /**
     * Adds a specific realm role to a user. This method finds the specified role within the realm and
     * assigns it to the user identified by userId.
     *
     * @param userId   The ID of the user to whom the role will be added.
     * @param roleName The name of the role to be added to the user.
     */
    public void addRealmRoleToUser(String userId, String roleName) {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId(); // Dynamically obtain the realm
        UsersResource usersResource = keycloak.realm(realm).users();
        UserResource userResource = usersResource.get(userId);
        List<RoleRepresentation> roles = keycloak.realm(realm).roles().list();

        RoleRepresentation roleToAdd = roles.stream()
                .filter(role -> role.getName().equals(roleName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Role not found: " + roleName));

        userResource.roles().realmLevel().add(Arrays.asList(roleToAdd));
        if (!keycloak.isClosed()) {
            keycloak.close();
        }
    }

    /**
     * Gets all client roles assigned to a specific user. This method fetches the roles from the
     * "dpex-frontend" client (identified by managedClientId) that are assigned to the user.
     *
     * @param userId The ID of the user whose client roles are to be retrieved.
     * @return A list of names of the client roles assigned to the user.
     */
    public List<String> getUserClientRoles(String userId) {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId();
        // Obtain reference to all clients in the realm
        List<ClientRepresentation> clients = keycloak.realm(realm).clients().findAll();

        // Find the client by its external client ID (e.g., "dpex-frontend")
        String managedClientUuid = clients.stream()
                .filter(client -> client.getClientId().equals(managedClientId)) // Match by external client ID
                .findFirst()
                .map(ClientRepresentation::getId) // Retrieve the internal UUID
                .orElseThrow(() -> new IllegalStateException("Managed client not found"));

        // Now fetch the roles using the internal UUID
        UserResource userResource = keycloak.realm(realm).users().get(userId);
        List<String> roles = userResource.roles().clientLevel(managedClientUuid).listEffective().stream()
                .map(RoleRepresentation::getName)
                .collect(Collectors.toList());
        if (!keycloak.isClosed()) {
            keycloak.close();
        }
        return roles;
    }


    /**
     * Adds a specific client role to a user within the current tenant's realm. This method identifies the
     * client based on the managed client ID and assigns the specified role to the user.
     *
     * @param userId   The ID of the user to whom the role will be added.
     * @param roleName The name of the client role to be added to the user.
     */
    public void addClientRoleToUser(String userId, String roleName) {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId();
        List<ClientRepresentation> clients = keycloak.realm(realm).clients().findAll();

        String clientUuid = clients.stream()
                .filter(client -> client.getClientId().equals(managedClientId)) // Match by external client ID
                .findFirst()
                .map(ClientRepresentation::getId) // Retrieve the internal UUID
                .orElseThrow(() -> new IllegalStateException("Client not found"));

        UserResource userResource = keycloak.realm(realm).users().get(userId);
        ClientResource clientResource = keycloak.realm(realm).clients().get(clientUuid);

        RoleRepresentation roleToAdd = clientResource.roles().get(roleName).toRepresentation();

        userResource.roles().clientLevel(clientUuid).add(List.of(roleToAdd));

        if (!keycloak.isClosed()) {
            keycloak.close();
        }
    }

    /**
     * Removes a specific client role from a user within the current tenant's realm. This method identifies
     * the client based on the managed client ID and removes the specified role from the user.
     *
     * @param userId   The ID of the user from whom the role will be removed.
     * @param roleName The name of the client role to be removed from the user.
     */
    public void removeClientRoleFromUser(String userId, String roleName) {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId();
        List<ClientRepresentation> clients = keycloak.realm(realm).clients().findAll();

        String clientUuid = clients.stream()
                .filter(client -> client.getClientId().equals(managedClientId)) // Match by external client ID
                .findFirst()
                .map(ClientRepresentation::getId) // Retrieve the internal UUID
                .orElseThrow(() -> new IllegalStateException("Client not found"));

        UserResource userResource = keycloak.realm(realm).users().get(userId);
        ClientResource clientResource = keycloak.realm(realm).clients().get(clientUuid);

        RoleRepresentation roleToRemove = clientResource.roles().get(roleName).toRepresentation();

        userResource.roles().clientLevel(clientUuid).remove(List.of(roleToRemove));

        if (!keycloak.isClosed()) {
            keycloak.close();
        }
    }

    /**
     * Retrieves the account details attribute for a specified user. The method fetches user information and
     * extracts the account details from custom attributes.
     *
     * @param userId The ID of the user whose account details are to be retrieved.
     * @return A list of maps containing account details.
     * @throws IOException If there's an error in reading the account details.
     */
    public List<Map<String, String>> getAccountDetails(String userId) throws IOException {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId();
        UserResource userResource = keycloak.realm(realm).users().get(userId);
        UserRepresentation userRepresentation = userResource.toRepresentation();
        Map<String, List<String>> attributes = userRepresentation.getAttributes();

        if (attributes == null || !attributes.containsKey("accountDetails")) {
            throw new IllegalStateException("accountDetails attribute not found for user " + userId);
        }

        String jsonAttribute = attributes.get("accountDetails").get(0);
        ObjectMapper mapper = new ObjectMapper();

        List<Map<String, String>> accountDetails = mapper.readValue(jsonAttribute, new TypeReference<List<Map<String, String>>>() {
        });
        if (!keycloak.isClosed()) {
            keycloak.close();
        }
        return accountDetails;
    }

    /**
     * Updates the account details attribute for a specified user. This method allows for updating
     * custom user attributes in the realm (in the current version used for BPM IDs).
     *
     * @param userId            The ID of the user whose account details are to be updated.
     * @param newAccountDetails The new account details to be set for the user.
     * @throws IOException If there's an error in mapping the account details to JSON format.
     */
    public void updateAccountDetails(String userId, List<Map<String, String>> newAccountDetails) throws IOException {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId();
        UserResource userResource = keycloak.realm(realm).users().get(userId);
        UserRepresentation userRepresentation = userResource.toRepresentation();
        Map<String, List<String>> attributes = userRepresentation.getAttributes();

        ObjectMapper mapper = new ObjectMapper();
        String jsonAttribute = mapper.writeValueAsString(newAccountDetails);

        if (attributes != null) {
            attributes.put("accountDetails", Collections.singletonList(jsonAttribute));
        } else {
            userRepresentation.setAttributes(Collections.singletonMap("accountDetails", Collections.singletonList(jsonAttribute)));
        }

        userResource.update(userRepresentation);

        if (!keycloak.isClosed()) {
            keycloak.close();
        }
    }


    /**
     * Prints all clients in the current realm to the standard output. This method is primarily used
     * for debugging or logging purposes to list all clients within the tenant's realm.
     */
    public void printAllClients() {
        Keycloak keycloak = getKeycloakClient();
        String realm = TenantContext.getTenantId();
        RealmResource realmResource = keycloak.realm(realm);
        ClientsResource clientsResource = realmResource.clients();

        List<ClientRepresentation> clients = clientsResource.findAll();
        System.out.println("Printing all clients.");

        for (ClientRepresentation client : clients) {
            System.out.println("Client ID: " + client.getId() + ", Name: " + client.getName());
        }

        if (!keycloak.isClosed()) {
            keycloak.close();
        }
    }

    /**
     * This method retrieves all users from Keycloak, checks if their BPM IDs match any BPM IDs in the provided
     * set of DPEX users, and upgrades their roles to "dpex_member" if a match is found.
     * <p>
     * This method is intended to be used when a process model is uploaded, ensuring that users associated
     * with the process model are granted the necessary roles for access or management within the system.
     *
     * @param users A set of users (from the organizational model) whose roles might be upgraded. These users
     *              should have valid BPM IDs that can be matched against Keycloak user BPM IDs.
     *              <p>
     *              Usage:
     *              Set<User> usersFromModel = fetchUsersFromOrganizationalModel(someModel);
     *              autoUpgradeUsersFromProcessModel(usersFromModel);
     */
    public void autoUpgradeUsersFromProcessModel(Set<User> users) {
        List<UserRepresentation> keycloakUsers = getUsers();

        for (UserRepresentation keycloakUser : keycloakUsers) {
            List<String> keycloakUserBpmIds = extractBpmIds(keycloakUser);

            Set<User> finalUsers = users;
            boolean matchFound = keycloakUserBpmIds.stream().anyMatch(
                    bpmId -> finalUsers.stream().anyMatch(
                            user -> bpmId.equals(user.getBpmId())
                    )
            );
            // If a match is found, upgrade the user's role
            if (matchFound) {
                addClientRoleToUser(keycloakUser.getId(), "dpex_member");
            }
        }
    }
}
