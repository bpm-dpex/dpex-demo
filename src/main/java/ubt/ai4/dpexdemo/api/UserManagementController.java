package ubt.ai4.dpexdemo.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ubt.ai4.dpexdemo.db.EncryptedEthereumKey;
import ubt.ai4.dpexdemo.db.EncryptedEthereumKeyRepository;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Provides RESTful web services for managing users within a tenant (realm) in Keycloak. It allows
 * for listing users, managing user roles, and handling user-specific data such as account details (currently BPM IDs).
 */

@CrossOrigin
@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserManagementController {

    private final KeycloakAdminService keycloakAdminService;

    @Autowired
    private EncryptedEthereumKeyRepository keyRepository;

    @Data
    @AllArgsConstructor
    class UserDto {
        private String userId;
        private String username;
        private String firstName;
        private String lastName;
    }

    @Data
    class UserRoleDto {
        private String userId;
        private String roleName;
    }

    private final Environment env;

    // Util Methods
    private boolean isDevProfileActive() {
        return Arrays.asList(env.getActiveProfiles()).contains("dev");
    }

    /**
     * Retrieves a list of all users within the current tenant. Only accessible by users with the 'admin' role of the current tenant's realm.
     *
     * @return ResponseEntity containing a list of user DTOs.
     */
    @PreAuthorize("hasRole('admin')")
    @GetMapping("")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        List<UserRepresentation> users = keycloakAdminService.getUsers();
        List<UserDto> userDtos = users.stream()
                .map(user -> new UserDto(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName()))
                .collect(Collectors.toList());

        return ResponseEntity.ok(userDtos);
    }

    /**
     * Retrieves a list of client roles assigned to a specific user. Only accessible by users with the 'admin' role of the current tenant's realm.
     *
     * @param userId The unique ID of the user whose client roles are being requested.
     * @return ResponseEntity containing a list of role names.
     */
    @PreAuthorize("hasRole('admin')")
    @GetMapping("/{userId}/client-roles")
    public ResponseEntity<List<String>> getUserClientRoles(@PathVariable String userId) {
        List<String> roles = keycloakAdminService.getUserClientRoles(userId);
        return ResponseEntity.ok(roles);
    }

    /**
     * Adds a client role to a specific user. Only accessible by users with the 'admin' role of the current tenant's realm.
     *
     * @param userId   The unique ID of the user to whom the role will be added.
     * @param roleName The name of the role to be added.
     * @return ResponseEntity indicating the operation's success status.
     */
    @PreAuthorize("hasRole('admin')")
    @PostMapping("/{userId}/add-client-roles")
    public ResponseEntity<Void> addClientRoleToUser(@PathVariable String userId, @RequestBody String roleName) {
        String cleanedRoleName = roleName.replace("\"", "");
        keycloakAdminService.addClientRoleToUser(userId, cleanedRoleName);

        return ResponseEntity.ok().build();
    }

    /**
     * Removes a client role from a specific user. Only accessible by users with the 'admin' role of the current tenant's realm.
     *
     * @param userId   The unique ID of the user from whom the role will be removed.
     * @param roleName The name of the role to be removed.
     * @return ResponseEntity indicating the operation's success status.
     */
    @PreAuthorize("hasRole('admin')")
    @PostMapping("/{userId}/remove-client-roles")
    public ResponseEntity<Void> removeClientRoleFromUser(@PathVariable String userId, @RequestBody String roleName) {
        String cleanedRoleName = roleName.replace("\"", "");
        keycloakAdminService.removeClientRoleFromUser(userId, cleanedRoleName);

        return ResponseEntity.ok().build();
    }

    /**
     * Fetches the account details for a specific user. Only accessible by users with the 'admin' role of the current tenant's realm.
     *
     * @param userId The unique ID of the user whose account details are being requested.
     * @return ResponseEntity containing the account details.
     */
    @PreAuthorize("hasRole('admin')")
    @GetMapping("/{userId}/account-details")
    public ResponseEntity<List<Map<String, String>>> getAccountDetails(@PathVariable String userId) {
        try {
            List<Map<String, String>> accountDetails = keycloakAdminService.getAccountDetails(userId);
            return ResponseEntity.ok(accountDetails);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Updates the account details for a specific user. Only accessible by users with the 'admin' role of the current tenant's realm.
     *
     * @param userId            The unique ID of the user whose account details are to be updated.
     * @param newAccountDetails The new account details to be set for the user.
     * @return ResponseEntity indicating the operation's success status.
     */
    @PreAuthorize("hasRole('admin')")
    @PostMapping("/{userId}/account-details")
    public ResponseEntity<Void> updateAccountDetails(@PathVariable String userId, @RequestBody List<Map<String, String>> newAccountDetails) {
        try {
            keycloakAdminService.updateAccountDetails(userId, newAccountDetails);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Utility method to extract BPM IDs from a user's account details. This method parses the account details
     * from a JSON structure and extracts the BPM ID values.
     *
     * @param user The user representation from Keycloak.
     * @return A list of BPM IDs extracted from the user's account details.
     */
    public static List<String> extractBpmIds(UserRepresentation user) {
        List<String> bpmIds = new ArrayList<>();
        // ObjectMapper for parsing JSON
        ObjectMapper objectMapper = new ObjectMapper();
        if (user.getAttributes() != null) {
            List<String> accountDetails = user.getAttributes().get("accountDetails");
            if (accountDetails != null && !accountDetails.isEmpty()) {
                String json = accountDetails.get(0);
                try {
                    // Parse the JSON string
                    List<Map<String, String>> details = objectMapper.readValue(json, new TypeReference<List<Map<String, String>>>() {
                    });

                    // Extract BPM IDs
                    for (Map<String, String> detail : details) {
                        String bpmId = detail.get("bpmId");
                        if (bpmId != null) {
                            bpmIds.add(bpmId);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return bpmIds;
    }


    /**
     * Saves a new encrypted ethereum private key associated with a specific BPM ID for a user.
     *
     * @param username The username for which the encrypted ethereum private key is being saved. The method checks if the current
     *                 authenticated user matches this username.
     * @param bpmID    The BPM ID to which the secret relates.
     * @param secret   The encrypted ethereum private key to be stored.
     * @return A ResponseEntity representing the outcome of the operation.
     * - 200 OK if the encrypted ethereum private key is saved successfully.
     * - 403 Forbidden if the current user does not match the username.
     * - 500 Internal Server Error if there are any exceptions during the operation.
     */
    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @PostMapping("/{username}/save-secret")
    public ResponseEntity<?> saveSecret(@PathVariable String username, @RequestParam String bpmID, @RequestParam String secret) {
        if (!isDevProfileActive() && !currentUserMatches(username)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Access denied you are not logged in as the user whose secrets you are trying to request!.");
        }

        EncryptedEthereumKey key = keyRepository.findById(username).orElse(new EncryptedEthereumKey());
        key.setUsername(username);

        try {
            TreeMap<String, String> secretsMap = key.getSecretsMap() != null ? key.getSecretsMap() : new TreeMap<>();
            secretsMap.put(bpmID, secret);
            key.setSecretsMap(secretsMap);
            keyRepository.save(key);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Retrieves an encrypted ethereum private key associated with a specific BPM ID for a given user.
     *
     * @param username The username for which the encrypted ethereum private key retrieval is requested.
     * @param bpmID    The BPM ID related to the encrypted ethereum private key.
     * @return A ResponseEntity containing the secret or an error message:
     * - 200 OK with the encrypted ethereum private key in the body if found.
     * - 403 Forbidden if the user requesting is not the user in question or an admin.
     * - 404 Not Found if no encrypted ethereum private key is found for the given bpmID.
     * - 500 Internal Server Error if there are any exceptions during retrieval.
     */
    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("/{username}/get-secret")
    public ResponseEntity<?> getSecret(@PathVariable String username, @RequestParam String bpmID) {
        if (!isDevProfileActive() && !currentUserMatches(username)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Access denied you are not logged in as the user whose secrets you are trying to request!.");
        }

        return keyRepository.findById(username)
                .map(key -> {
                    try {
                        Map<String, String> secretsMap = key.getSecretsMap();
                        String secret = secretsMap.get(bpmID);
                        return ResponseEntity.ok(secret);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return ResponseEntity.internalServerError().build();
                    }
                }).orElse(ResponseEntity.notFound().build());
    }

    /**
     * Helper method to check if the current authenticated user matches the provided username.
     *
     * @param username The username to check against the current authenticated user.
     * @return true if the usernames match
     */
    private boolean currentUserMatches(String username) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName().equalsIgnoreCase(username);
    }
}
