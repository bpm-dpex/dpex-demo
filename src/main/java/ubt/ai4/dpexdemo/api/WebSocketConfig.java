package ubt.ai4.dpexdemo.api;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import ubt.ai4.dpexdemo.config.JwtHandshakeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import java.util.Arrays;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

  private final JwtHandshakeInterceptor jwtHandshakeInterceptor;
  private final Environment environment;

  // Inject the JwtHandshakeInterceptor and Environment
  @Autowired
  public WebSocketConfig(JwtHandshakeInterceptor jwtHandshakeInterceptor, Environment environment) {
    this.jwtHandshakeInterceptor = jwtHandshakeInterceptor;
    this.environment = environment;
  }

  @Override
  public void configureMessageBroker(MessageBrokerRegistry config) {
    config.enableSimpleBroker("/user", "/topic");
  }

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    // Check if 'dev' profile is active
    boolean isDevProfileActive = Arrays.asList(environment.getActiveProfiles()).contains("dev");

    if (!isDevProfileActive) {
      // Only add JwtHandshakeInterceptor if 'dev' profile is not active
      registry.addEndpoint("/dpex-ws")
              .setAllowedOrigins("*")
              .addInterceptors(jwtHandshakeInterceptor);
    } else {
      // Do not add JwtHandshakeInterceptor in 'dev' profile
      registry.addEndpoint("/dpex-ws")
              .setAllowedOrigins("*");
    }
  }

}


