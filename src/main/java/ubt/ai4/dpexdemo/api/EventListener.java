package ubt.ai4.dpexdemo.api;

import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.db.AllianceRepository;
import dpex.error.TaskNotFoundException;
import dpex.events.SendInstantiationToFrontendEvent;
import dpex.events.SendTaskActionToFrontendEvent;
import dpex.events.TaskActionEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import com.google.common.eventbus.Subscribe;
import dpex.action.TaskAction;
import dpex.communication.IncomingMessageEvent;
import dpex.events.InstantiationEvent;
import ubt.ai4.dpexdemo.utils.ScriptExecutor;

/**
 * Listens to and handles various events related to task actions, user messaging, and process instantiation.
 * It receives events from the event bus and forwards them to the frontend via WebSocket
 */
@Component
@RequiredArgsConstructor
public class EventListener {
	
    private final SimpMessagingTemplate simpMessagingTemplate;
	private final AllianceRepository allianceRepository;

    /**
     * This event gets called after an instantiation. Creating camunda users matching the given organizationalModel. After the user creation all automated tasks
     * get executed
     *
     * @param event
     * @throws TaskNotFoundException
     */
    @Subscribe
    public void instantiationEvent(InstantiationEvent event) throws TaskNotFoundException {
        simpMessagingTemplate.convertAndSend("/topic/instantiation/" + event.getTenantID() + "/" + event.getGar(), event.getInstance());

		//TODO: Causes exception (No group found with id 'camunda-admin'.: group is null)
		/*
		//Create camunda users
		//This code should obviously not be executed when the client is not using camunda
		CamundaManager cm = new CamundaManager();
		List<Alliance> alliances = allianceRepository.findAll();
		for(Alliance a: alliances){
			CamundaBPMEngine engine = (CamundaBPMEngine) a.getEngine();
			Set<User> users = a.getModel().getParticipants();
			cm.createCamundaUsers(engine, users);
		}*/

		//Manage scripts
		ScriptExecutor executor = new ScriptExecutor(allianceRepository);
		executor.manageScripts(event);

        //...
    }

    /**
     * Processes task action events and broadcasts them to the appropriate topic (users of the current tenant) for frontend updates.
     *
     * @param taskActionEvent The event encapsulating a task action, which includes details like tenant ID and task identifiers.
     */
    @Subscribe
    public void taskActionEvent(TaskActionEvent taskActionEvent) {
        simpMessagingTemplate.convertAndSend("/topic/taskAction/" + taskActionEvent.getTenantID() + "/" + taskActionEvent.getTaskaction().getInstance().getGlobalInstanceReference(), taskActionEvent.getTaskaction().getTask().getActivity());
    }

    /**
     * Handles incoming message events and routes them to specific users based on access tokens.
     *
     * @param incomingMessageEvent The event containing the message and the recipient's access token.
     */
    @Subscribe
    public void incomingMessageEvent(IncomingMessageEvent incomingMessageEvent) {
        simpMessagingTemplate.convertAndSendToUser(incomingMessageEvent.getAccessToken(), "/queue", incomingMessageEvent.getIncomingMessageDTO());
    }

    /**
     * Sends task action events to the frontend for the user to sign them with their private Ethereum key
     *
     * @param event The task action event to be sent to the frontend.
     */
    @Subscribe
    public void incomingSendTaskActionEvent(SendTaskActionToFrontendEvent event) {
        simpMessagingTemplate.convertAndSend("/topic/signTransaction/taskAction/" + event.getTenantID() + event.getSciID(), event);
    }

    /**
     * Sends instantiation events to the frontend for the user to sign them with their private Ethereum key
     *
     * @param event The instantiation event to be sent to the frontend.
     */
    @Subscribe
    public void incomingSendInstantiationEvent(SendInstantiationToFrontendEvent event) {
        simpMessagingTemplate.convertAndSend("/topic/signTransaction/instantiation/" + event.getTenantID() + event.getSciID(), event);
    }


    /**
     * This event gets called to check for automated tasks after a successful complete or claim (returning immediately in case of a CLAIM)
     *
     * @param taskAction
     * @throws TaskNotFoundException
     */
    @Subscribe
    public void checkForScripting(TaskAction taskAction) throws TaskNotFoundException {
        if (taskAction.getTaskLifeCycleStage() == TaskLifeCycleStage.CLAIM)
            return;
        ScriptExecutor executor = new ScriptExecutor(allianceRepository);
        executor.manageScripts(taskAction);
    }

}
