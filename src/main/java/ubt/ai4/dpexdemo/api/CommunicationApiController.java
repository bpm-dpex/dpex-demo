package ubt.ai4.dpexdemo.api;

import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;

import dpex.bpm.execution.BPMEngine;
import dpex.bpm.execution.EventLog;
import dpex.bpm.execution.Instance;
import dpex.communication.DPEXCommunicationService;
import dpex.communication.GetRoomMessageDTO;
import dpex.communication.RoomDTO;
import dpex.core.Alliance;
import dpex.core.Event;
import dpex.db.AllianceRepository;
import dpex.db.UserRepository;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import ubt.ai4.dpexdemo.camunda.org.CamundaMPModel;
import ubt.ai4.dpexdemo.org.OrgUtils;

/**
 * The CommunicationApiController provides REST API endpoints for managing communications
 * within the DPEX system, such as sending messages, managing rooms, and handling user logins and logouts
 * for communication services.
 *
 * This controller uses the "X-Communication-Token" to authorize users for actions in communication services
 * such as Matrix or Discord. This token is distinct from the access token used to authenticate users in this DPEX-Demo
 * Java Backend itself. The "X-Communication-Token" is specifically for interacting with external communication
 * microservices and is managed separately to ensure that communication actions are securely authenticated
 * independent of the backend session control.
 *
 * Endpoints in this controller handle various tasks like message sending, room creation, and session management,
 * relying on the DPEX Communication Service to perform the actual communication logic.
 */
@CrossOrigin
@RestController
@RequestMapping("api/communication")
@RequiredArgsConstructor
public class CommunicationApiController {
    private final DPEXCommunicationService dpexCommunicationService;
    private final AllianceRepository allianceRepository;
    private final UserRepository userRepository;

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody LoginDTO loginDTO){
    	String accessToken = dpexCommunicationService.login(
    		loginDTO.getUserId(),
    		loginDTO.getPassword(),
    		userRepository.findFirstByCommunicationId(loginDTO.getUserId()).orElseThrow().getHomeServerUrl(),
    		loginDTO.getCommunicationModule()
    	);
    	return ResponseEntity.ok(accessToken);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "logout", method = RequestMethod.DELETE)
    public ResponseEntity<?> logout(@RequestHeader("X-Communication-Token") String accessToken, @RequestParam String communicationModule){
    	dpexCommunicationService.logout(
    		accessToken,
    		communicationModule
    	);
    	return ResponseEntity.ok(null);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "sendDirectMessage", method = RequestMethod.POST)
    public ResponseEntity<?> sendDirectMessage(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody DirectMessageDTO directMessageDTO){
    	String[] roomIds = new String[directMessageDTO.getRecipients().length];
    	int i = 0;
    	for(String recipient : directMessageDTO.getRecipients())
	    	roomIds[i++] = dpexCommunicationService.sendDirectMessage(
	    		accessToken,
	    		recipient,
	    		directMessageDTO.getMessage(),
	    		directMessageDTO.getGar(),
	    		directMessageDTO.getFile(),
	    		directMessageDTO.isFileIsImage()
	    	);
    	return ResponseEntity.ok(roomIds);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "rooms", method = RequestMethod.GET)
    public ResponseEntity<?> getRooms(@RequestHeader("X-Communication-Token") String accessToken, @RequestParam String communicationModule){
    	RoomDTO[] response = dpexCommunicationService.getRooms(
    		accessToken,
    		communicationModule
    	);
    	return ResponseEntity.ok(response);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "roomMessages", method = RequestMethod.GET)
    public ResponseEntity<?> getRoomMessages(@RequestHeader("X-Communication-Token") String accessToken, @RequestParam String communicationModule, @RequestParam String roomId){
    	GetRoomMessageDTO[] response = dpexCommunicationService.getRoomMessages(
    		accessToken,
    		roomId,
    		communicationModule
    	);
    	return ResponseEntity.ok(response);
    }
	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "roomMembers", method = RequestMethod.GET)
    public ResponseEntity<?> getRoomMembers(@RequestHeader("X-Communication-Token") String accessToken, @RequestParam String gar, @RequestParam String roomId){
    	String[] members = dpexCommunicationService.getRoomMembers(accessToken, roomId, gar);
    	return ResponseEntity.ok(members);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "createSpace", method = RequestMethod.POST)
    public ResponseEntity<?> createSpace(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody GarDTO garDTO){
    	String spaceId = dpexCommunicationService.createSpace(
			accessToken,
			garDTO.getGar()
    	);
    	return ResponseEntity.ok(spaceId);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "createRoom", method = RequestMethod.POST)
    public ResponseEntity<?> createRoom(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody RoomWithDirectInviteDTO roomWithDirectInviteDTO){
    	String roomId = dpexCommunicationService.createRoom(
    		accessToken,
    		roomWithDirectInviteDTO.getSpaceId(),
    		roomWithDirectInviteDTO.getName(),
    		roomWithDirectInviteDTO.getInvite(),
    		roomWithDirectInviteDTO.getGar()
    	);
    	return ResponseEntity.ok(roomId);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "createRoomByOrgQuery", method = RequestMethod.POST)
    public ResponseEntity<?> createRoomByOrgQuery(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody RoomWithOrgQueryDTO roomWithOrgQueryDTO){
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(roomWithOrgQueryDTO.getGar()).orElseThrow();
    	CamundaMPModel camundaMPModel = (CamundaMPModel)alliance.getModel();
    	String organizationalModel = camundaMPModel.getOrganizationalModel();
    	OrgQueryDTO orgQuery = roomWithOrgQueryDTO.getOrgQuery();
    	String[] invite = OrgUtils.getBpmIdsByQuery(organizationalModel, orgQuery.getRoles(), orgQuery.getDepartments());
    	String roomId = dpexCommunicationService.createRoom(
    		accessToken,
    		roomWithOrgQueryDTO.getSpaceId(),
    		roomWithOrgQueryDTO.getName(),
    		invite,
    		roomWithOrgQueryDTO.getGar()
    	);
    	return ResponseEntity.ok(roomId);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "sendRoomMessage", method = RequestMethod.POST)
    public ResponseEntity<?> sendRoomMessage(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody RoomMessageDTO roomMessageDTO){
    	dpexCommunicationService.sendRoomMessage(
    		accessToken,
    		roomMessageDTO.getRoomId(),
    		roomMessageDTO.getMessage(),
    		roomMessageDTO.getCommunicationModule(),
    		roomMessageDTO.getFile(),
    		roomMessageDTO.isFileIsImage()
    	);
    	return ResponseEntity.ok(null);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "sendDirectMessageByOrgQuery", method = RequestMethod.POST)
    public ResponseEntity<?> sendDirectMessageByOrgQuery(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody DirectMessageWithOrgQueryDTO directMessageWithOrgQueryDTO){
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(directMessageWithOrgQueryDTO.getGar()).orElseThrow();
    	CamundaMPModel camundaMPModel = (CamundaMPModel)alliance.getModel();
    	String organizationalModel = camundaMPModel.getOrganizationalModel();
    	OrgQueryDTO orgQuery = directMessageWithOrgQueryDTO.getOrgQuery();
    	String[] recipients = OrgUtils.getBpmIdsByQuery(organizationalModel, orgQuery.getRoles(), orgQuery.getDepartments());
    	for(String recipient : recipients) {
    		dpexCommunicationService.sendDirectMessage(
    			accessToken,
    			recipient,
    			directMessageWithOrgQueryDTO.getMessage(),
    			directMessageWithOrgQueryDTO.getGar(),
    			directMessageWithOrgQueryDTO.getFile(),
    			directMessageWithOrgQueryDTO.isFileIsImage()
    		);
    	}
    	return ResponseEntity.ok(null);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "inviteToRoom", method = RequestMethod.POST)
    public ResponseEntity<?> inviteToRoom(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody InviteDTO inviteDTO){
    	dpexCommunicationService.inviteToRoom(
    		accessToken,
    		inviteDTO.getRoomId(),
    		inviteDTO.getInvite(),
    		inviteDTO.getGar()
    	);
    	return ResponseEntity.ok(null);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "inviteToRoomByOrgQuery", method = RequestMethod.POST)
    public ResponseEntity<?> inviteToRoomByOrgQuery(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody InviteWithOrgQueryDTO inviteWithOrgQueryDTO){
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(inviteWithOrgQueryDTO.getGar()).orElseThrow();
    	CamundaMPModel camundaMPModel = (CamundaMPModel)alliance.getModel();
    	String organizationalModel = camundaMPModel.getOrganizationalModel();
    	OrgQueryDTO orgQuery = inviteWithOrgQueryDTO.getOrgQuery();
    	String[] invite = OrgUtils.getBpmIdsByQuery(organizationalModel, orgQuery.getRoles(), orgQuery.getDepartments());
    	dpexCommunicationService.inviteToRoom(
    		accessToken,
    		inviteWithOrgQueryDTO.getRoomId(),
    		invite,
    		inviteWithOrgQueryDTO.getGar()
    	);
    	return ResponseEntity.ok(null);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "leaveRoom", method = RequestMethod.POST)
    public ResponseEntity<?> leaveRoom(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody RoomIdWithCommunicationModuleDTO leaveRoomDTO){
    	dpexCommunicationService.leaveRoom(
    		accessToken,
    		leaveRoomDTO.getRoomId(),
    		leaveRoomDTO.getCommunicationModule()
    	);
    	return ResponseEntity.ok(null);
    }

	@PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @RequestMapping(value = "sendMessageToTaskExecutor", method = RequestMethod.POST)
    public ResponseEntity<?> sendMessageToTaskExecutor(@RequestHeader("X-Communication-Token") String accessToken, @RequestBody MessageToTaskExecutorDTO messageToTaskExecutorDTO){
    	Alliance alliance = allianceRepository.findByGlobalInstanceReference(messageToTaskExecutorDTO.getGir()).orElseThrow();
    	BPMEngine bpmEngine = alliance.getEngine();
    	Set<Instance> instances = bpmEngine.getInstances();
    	Instance[] filteredInstances = instances.stream().filter(instance ->
    		instance.getGlobalInstanceReference().equals(messageToTaskExecutorDTO.getGir())
    	).toArray(Instance[]::new);
    	if(filteredInstances.length == 0)
    		return ResponseEntity.badRequest().body("Instance not found");
    	//Since the gir is unique the array definitely has size 1
    	Instance instance = filteredInstances[0];
    	EventLog eventLog = instance.getEventLog();
    	Set<Event> events = eventLog.getEvents();
    	Event[] filteredEvents = events.stream().filter(event ->
    		event.getActivity().equals(messageToTaskExecutorDTO.getTask())
    	).toArray(Event[]::new);
    	if(filteredEvents.length == 0)
    		return ResponseEntity.badRequest().body("Task not found");
    	//Since the task name must be unique the array definitely has size 1
    	Event event = filteredEvents[0];
    	String bpmId = event.getResource();
    	dpexCommunicationService.sendDirectMessage(
    		accessToken,
    		bpmId,
    		messageToTaskExecutorDTO.getMessage(),
    		alliance.getGlobalAllianceReference(),
    		messageToTaskExecutorDTO.getFile(),
    		messageToTaskExecutorDTO.isFileIsImage()
    	);
    	return ResponseEntity.ok(null);
    }
}

@Data
class GarDTO{
	@JsonProperty(required = true)
	private String gar;
}

@Data
class CommunicationModuleDTO{
	@JsonProperty(required = true)
	private String communicationModule;
}

@Data
class MessageToTaskExecutorDTO{
	@JsonProperty(required = true)
	private String gir;
	
	@JsonProperty(required = true)
	private String task;
	
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = false)
	private String file;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}

@Data
@EqualsAndHashCode(callSuper = true)
class RoomIdWithCommunicationModuleDTO extends CommunicationModuleDTO{
	@JsonProperty(required = true)
	private String roomId;
}

@Data
@EqualsAndHashCode(callSuper = true)
class InviteDTO extends GarDTO{
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	//bpmIds
	private String[] invite;
}

@Data
@EqualsAndHashCode(callSuper = true)
class InviteWithOrgQueryDTO extends GarDTO{
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	private OrgQueryDTO orgQuery;
}

@Data
@EqualsAndHashCode(callSuper = true)
class RoomWithDirectInviteDTO extends GarDTO{
	@JsonProperty(required = true)
	private String spaceId;
	
	@JsonProperty(required = true)
	private String name;
	
	@JsonProperty(required = true)
	//bpmIds
	private String[] invite;
}

@Data
@EqualsAndHashCode(callSuper = true)
class RoomWithOrgQueryDTO extends GarDTO{
	@JsonProperty(required = true)
	private String spaceId;
	
	@JsonProperty(required = true)
	private String name;
	
	@JsonProperty(required = true)
	private OrgQueryDTO orgQuery;
}

@Data
@EqualsAndHashCode(callSuper = true)
class DirectMessageDTO extends GarDTO{
	@JsonProperty(required = true)
	//bpmIds
	private String[] recipients;
	
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = false)
	private String file;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}

@Data
@EqualsAndHashCode(callSuper = true)
class DirectMessageWithOrgQueryDTO extends GarDTO{
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = true)
	private OrgQueryDTO orgQuery;
	
	@JsonProperty(required = false)
	private String file;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}

@Data
@EqualsAndHashCode(callSuper = true)
class RoomMessageDTO extends CommunicationModuleDTO{
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = false)
	private String file;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}

@Data
@EqualsAndHashCode(callSuper = true)
class LoginDTO extends CommunicationModuleDTO{
	@JsonProperty(required = true)
	private String userId;
	
	@JsonProperty(required = true)
	private String password;
}

@Data
class OrgQueryDTO{
	@JsonProperty(required = false)
	private String[] roles;
	
	@JsonProperty(required = false)
	private String[] departments;
}