package ubt.ai4.dpexdemo.api;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import bftsmartcommunicationtypes.RequestType;
import dpex.impl.collaboration.bftsmart.BftSmartConfig;
import dpex.impl.collaboration.bftsmart.BftSmartFactory;
import dpex.impl.collaboration.bftsmart.BftSmartNetwork;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonProperty;

import dpex.action.Instantiation;
import dpex.bpm.execution.BPMEngineFactory;
import dpex.bpm.execution.BPMEngineType;
import dpex.bpm.execution.EventLog;
import dpex.bpm.execution.Instance;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.bpm.model.ProcessModelFactory;
import dpex.collaboration.CollaborationFactory;
import dpex.collaboration.CollaborationType;
import dpex.communication.CommunicationFactory;
import dpex.communication.CommunicationModuleType;
import dpex.core.Alliance;
import dpex.core.DPEXService;
import dpex.core.DPEXUtils;
import dpex.core.User;
import dpex.db.AllianceRepository;
import dpex.db.BPMEngineFactoryRepository;
import dpex.db.CollaborationFactoryRepository;
import dpex.db.CommunicationFactoryRepository;
import dpex.db.ProcessModelFactoryRepository;
import dpex.db.UserRepository;
import dpex.impl.bpm.model.bpmn.BPMNModel;
import dpex.impl.collaboration.ethereum.raw.EthereumRawConfig;
import dpex.impl.collaboration.ethereum.raw.EthereumRawFactory;
import dpex.impl.collaboration.hyperledger.HyperledgerFactory;
import dpex.impl.communication.DiscordAdapterFactory;
import dpex.impl.communication.MatrixAdapterFactory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import ubt.ai4.dpexdemo.camunda.org.CamundaBPMEngineFactory;
import ubt.ai4.dpexdemo.camunda.org.CamundaMPModel;
import ubt.ai4.dpexdemo.camunda.org.CamundaMPModelFactory;
import ubt.ai4.dpexdemo.malicious.MaliciousEngineFactory;
import ubt.ai4.dpexdemo.org.OrgUtils;
import ubt.ai4.dpexdemo.org.ParticipantDTO;
import ubt.ai4.dpexdemo.utils.ScriptManager;

import static net.gcardone.junidecode.Junidecode.*;

import ubt.ai4.dpexdemo.config.SecurityUtils;


import static ubt.ai4.dpexdemo.api.UserManagementController.extractBpmIds;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ApiController {
    // If you want to secure endpoints based on roles or bpm IDs:
    // @PreAuthorize("hasRole('admin') and @securityUtils.hasBpmId('YourSpecificBpmId')")

    private final KeycloakAdminService keycloakAdminService;
    private final ProcessModelFactoryRepository baseModelFactoryRepository;
    private final AllianceRepository allianceRepository;
    private final UserRepository userRepository;
    private final DPEXUtils dpexUtils;
    private final DPEXService dpexService;
    private final BPMEngineFactoryRepository bpmEngineFactoryRepository;
    private final CollaborationFactoryRepository collaborationFactoryRepository;
    private final CommunicationFactoryRepository communicationFactoryRepository;
    private final Environment env;

    // Util Methods
    private boolean isDevProfileActive() {
        return Arrays.asList(env.getActiveProfiles()).contains("dev");
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "ethereum/deploySCI", method = RequestMethod.POST)
    public ResponseEntity<?> deploySci(@RequestBody DeploySciDTO deploySciDTO) {

        String gar = dpexUtils.deploySci(deploySciDTO.getConnection(), deploySciDTO.getSciId(), deploySciDTO.getApiKey());
        return ResponseEntity.ok().body(gar);

    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "model", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<?> createProcessModel(ProcessModelDTO processModelDTO) {

        MultipartFile processModel = processModelDTO.getProcessModel();
        CamundaMPModelFactory modelFactory = new CamundaMPModelFactory();

        try {
            byte[] bpmnBytes = processModel.getBytes();
            String bpmnString = new String(bpmnBytes, StandardCharsets.UTF_8);

// In the case the dependency Unidecode is no longer maintained later, this less robust code can be used as an alternative:
//            // Replace German umlauts and sharp s
//            bpmnString = bpmnString.replace("ä", "ae")
//                    .replace("ö", "oe")
//                    .replace("ü", "ue")
//                    .replace("ß", "ss")
//                    .replace("Ä", "Ae")
//                    .replace("Ö", "Oe")
//                    .replace("Ü", "Ue");
//            bpmnString = unidecode(bpmnString);


            modelFactory.setBpmnModel(bpmnString.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        BPMNModel parsedBPMN = BPMNModel.fromString(new String(modelFactory.getBpmnModel()));

        modelFactory.setName(parsedBPMN.getProcessId());
        modelFactory.setOrganizationalModel(processModelDTO.getOrganizationalModel());

        Set<User> users = fetchUsersFromOrganizationaModel(processModelDTO.getOrganizationalModel());;
        if (processModelDTO.getAutoUpgradeUsers().equals("true")) {
            keycloakAdminService.autoUpgradeUsersFromProcessModel(users);
        }
        // Added to automatically Upgrade Users to DPEX member on model upload --------------------------------------


        /* avoid having a communication ID twice in the repository such that the request for retrieving the homeserver url
         * in the communication login function will always have a unique result
         * however, having empty string multiple times as communication ID is not problematic
         */
        users = users.stream().map(user -> {
            return userRepository.save(user);
//            if (user.getCommunicationId().equals("")) {
//                System.out.println(user);
//                return userRepository.save(user);
//            }
//            Optional<User> optionalUser = userRepository.findByCommunicationId(user.getCommunicationId());
//            if (optionalUser.isEmpty()) {
//                System.out.println(user);
//                return userRepository.save(user);
//            } else {
//                return optionalUser.get();
//            }
        }).collect(Collectors.toSet());

        /**
         * This user will be set as the executing one when executing automated tasks
         * He gets added as participant to make sure he also gets created as Camunda user
         */
        User localUser = new User();
        localUser.setBpmId("localUserForScriptExecution");
        userRepository.save(localUser);

        modelFactory.setParticipants(users);

        modelFactory = baseModelFactoryRepository.save(modelFactory);

        return ResponseEntity.ok().build();
    }

    private Set<User> fetchUsersFromOrganizationaModel(String organizationalModel) {
        String[] allLines = organizationalModel.split(OrgUtils.lineSplitter);
        //omitting the first line since it's the column names
        String[] lines = Arrays.copyOfRange(allLines, 1, allLines.length);

        return Arrays.stream(lines).map(line -> {
            String[] attributes = line.split(",");
            return User.builder()
                    .bpmId(attributes[OrgUtils.bpmIdIndex])
                    .sciId(attributes[OrgUtils.sciIdIndex])
                    .communicationId(attributes[OrgUtils.communicationIdIndex])
                    .homeServerUrl(attributes[OrgUtils.homeserverUrlIndex])
                    .build();
        }).collect(Collectors.toSet());
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "alliance", method = RequestMethod.POST)
    public ResponseEntity<?> createAlliance(@ModelAttribute AllianceDTO allianceDTO) {
        String factoryName = allianceDTO.getCollaboration();
        List<CollaborationFactory> allFactories;
        try {
            allFactories = collaborationFactoryRepository.findAll();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error retrieving factory information");
        }

        CollaborationFactory targetFactory = null;
        for (CollaborationFactory factory : allFactories) {
            if (factoryName.equals(factory.getName())) {
                targetFactory = factory;
                break;
            }
        }

        if (targetFactory == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Factory not found");
        }

        try {
            if (targetFactory instanceof EthereumRawFactory) {
                EthereumRawConfig config = EthereumRawConfig.builder().sciContractAddress(allianceDTO.getGAR()).blocksToWaitForFinality(Integer.parseInt(allianceDTO.getBlocksToWait())).allianceName(allianceDTO.getName()).build();
                Alliance alliance = dpexUtils.buildAlliance(
                        allianceDTO.getName(),
                        allianceDTO.getProcessModel(),
                        allianceDTO.getBpmEngine(),
                        allianceDTO.getCollaboration(),
                        allianceDTO.getCommunication(),
                        allianceDTO.getGAR(),
                        config
                );
                alliance = allianceRepository.save(alliance);
                return ResponseEntity.ok().body(alliance);
            } else if (targetFactory instanceof HyperledgerFactory) {
                System.out.println(factoryName + " is a HyperledgerFactory");
                // Assume HyperledgerFactory processing logic here...
                // Replace with appropriate return statement for HyperledgerFactory
            } else if (targetFactory instanceof BftSmartFactory) {
                BftSmartFactory bftsmartFactory = (BftSmartFactory) targetFactory;
                String[] parts = bftsmartFactory.getConnection().split(", ");
                int clientId = Integer.parseInt(parts[0]);
                List<String> newHosts = Arrays.asList(Arrays.copyOfRange(parts, 1, parts.length));

                BftSmartConfig bftconfig = BftSmartConfig.builder().clientId(clientId).ipConfig(newHosts).globalAllianceReference(allianceDTO.getGAR()).useDefaultKeys(bftsmartFactory.isUseDefaultKeys()).publicKeys(bftsmartFactory.getPublicKeys()).clientPrivateKey(bftsmartFactory.getClientPrivateKey()).clientPublicKey(bftsmartFactory.getClientPublicKey()).build();
                Alliance alliance2 = dpexUtils.buildAlliance(
                        allianceDTO.getName(),
                        allianceDTO.getProcessModel(),
                        allianceDTO.getBpmEngine(),
                        allianceDTO.getCollaboration(),
                        allianceDTO.getCommunication(),
                        allianceDTO.getGAR(),
                        bftconfig
                );
                Alliance allianceResp = allianceRepository.save(alliance2);
                // Called to potentially set up event listeners for already existing instances created by another communication partner
                ((BftSmartNetwork) alliance2.getCollaboration().getNetwork()).checkForExistingAlliance();
                Alliance allianceResp2 = allianceRepository.save(alliance2);
                return ResponseEntity.ok().body(allianceResp);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unknown factory type");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error processing the request");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error processing the request");
    }

    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("alliance")
    public ResponseEntity<?> deleteAlliance(@RequestParam String name) {
        for (Alliance alliance : allianceRepository.findAll()) {
            if (alliance.getName().equals(name)) {
                allianceRepository.delete(alliance);
                return ResponseEntity.noContent().build();
            }
        }
        return ResponseEntity.notFound().build();
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @PostMapping("instantiate")
    public ResponseEntity<?> instantiate(@RequestBody InstantiateDTO instantiateDTO) {
        if (!isDevProfileActive() && !SecurityUtils.hasBpmId(instantiateDTO.getBpmId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User does not have access to the requested bpmId!");
        }
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(instantiateDTO.getGar()).orElseThrow(() -> new RuntimeException());
        User user = OrgUtils.getUserFromBpmId(alliance.getModel(), instantiateDTO.getBpmId());
        Instantiation instantiation = dpexUtils.buildInstantiation(instantiateDTO.getGar(), alliance.getModel().getModelReference(), user, instantiateDTO.getVariables() != null ? instantiateDTO.getVariables() : "", instantiateDTO.getName());

        dpexService.sendInstantiation(alliance, instantiation);

        return ResponseEntity.ok().build();

    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("instances")
    public ResponseEntity<?> getInstances(@RequestParam String gar) {
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow(() -> new RuntimeException());
        // ((EthereumRawNetwork) alliance.getCollaboration().getNetwork()).getInstances();
        return ResponseEntity.ok().body(alliance.getEngine().getInstances());
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("instance")
    public ResponseEntity<?> getInstance(@RequestParam String gir) {
        Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow();
        Instance instance = alliance.getEngine().getInstances().stream().filter(instance_ -> instance_.getGlobalInstanceReference().equals(gir)).findFirst().orElseThrow();
        return ResponseEntity.ok(instance);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("alliances")
    public ResponseEntity<?> getAlliances(@RequestParam String bpmId) {
        if (!isDevProfileActive() && !SecurityUtils.hasBpmId(bpmId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User does not have access to the requested bpmId!");
        }
        List<Alliance> allAlliances = allianceRepository.findAll();

        //Filter only the alliances in which the given user is member of
        List<Alliance> alliances = new ArrayList<Alliance>();
        for (Alliance alliance : allAlliances)
            for (User participant : alliance.getModel().getParticipants())
                if (participant.getBpmId().equals(bpmId)) {
                    alliances.add(alliance);
                    if (alliance.getCollaboration().getNetwork() instanceof BftSmartNetwork) {
                        ((BftSmartNetwork) alliance.getCollaboration().getNetwork()).checkForExistingAlliance();
                        allianceRepository.save(alliance);
                    }
                    break;
                }


        GetAllianceDTO[] allianceDTOs = new GetAllianceDTO[alliances.size()];
        int i = 0;
        for (Alliance alliance : alliances) {
            String name = alliance.getName();
            String gar = alliance.getGlobalAllianceReference();
            String processModel = alliance.getModel().getModelReference();

            CamundaMPModel camundaMPModel = (CamundaMPModel) alliance.getModel();
            String organizationalModel = camundaMPModel.getOrganizationalModel();
            ParticipantDTO[] participants = OrgUtils.getAllParticipantsWithRolesAndDepartments(organizationalModel);

            Set<Instance> instances = alliance.getEngine().getInstances();

            String communicationModule = null;
            if (alliance.getCommunication() != null)
                communicationModule = alliance.getCommunication().getName();
            String collaboration = alliance.getCollaboration().getName();
            String bpmEngine = alliance.getEngine().getName();
            allianceDTOs[i++] = new GetAllianceDTO(name, gar, processModel, participants, instances.toArray(Instance[]::new), communicationModule, collaboration, bpmEngine);
        }
        return ResponseEntity.ok(allianceDTOs);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("myCommunicationModules")
    public ResponseEntity<?> getMyCommunicationModules(@RequestParam String bpmId) {
        if (!isDevProfileActive() && !SecurityUtils.hasBpmId(bpmId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User does not have access to the requested bpmId!");
        }
        Set<String> communicationModules = new HashSet<String>();
        List<Alliance> alliances = allianceRepository.findAll();
        for (Alliance alliance : alliances) {
            Set<User> allUsersOfAlliance = alliance.getModel().getParticipants();
            for (User userOfAlliance : allUsersOfAlliance) {
                if (userOfAlliance.getBpmId().equals(bpmId)) {
                    if (alliance.getCommunication() != null) {
                        String communicationModule = alliance.getCommunication().getName();
                        communicationModules.add(communicationModule);
                    }
                    break;
                }
            }
        }
        return ResponseEntity.ok(communicationModules.toArray());
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("collaborationTypes")
    public ResponseEntity<?> getCollaborationTypes() {
        EnumValueDTO[] types = Arrays.stream(CollaborationType.values()).map(type -> new EnumValueDTO(type.name(), type.label)).toArray(EnumValueDTO[]::new);
        return ResponseEntity.ok(types);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("bpmEngineTypes")
    public ResponseEntity<?> getBpmEngineTypes() {
        EnumValueDTO[] types = Arrays.stream(BPMEngineType.values()).map(type -> new EnumValueDTO(type.name(), type.label)).toArray(EnumValueDTO[]::new);
        return ResponseEntity.ok(types);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("communicationModuleTypes")
    public ResponseEntity<?> getCommunicationModuleTypes() {
        EnumValueDTO[] types = Arrays.stream(CommunicationModuleType.values()).map(type -> new EnumValueDTO(type.name(), type.label)).toArray(EnumValueDTO[]::new);
        return ResponseEntity.ok(types);
    }

    @PreAuthorize("hasRole('admin')")
    @PostMapping("collaboration")
    public ResponseEntity<?> createCollaboration(@RequestBody CollaborationConnectorDTO connectorDTO) {
        if (connectorDTO.getConnection() == null || connectorDTO.getConnection().equals(""))
            throw new RuntimeException("A collaboration always requires a connection");
        CollaborationType type = CollaborationType.valueOf(connectorDTO.getType());
        switch (type) {
            case ETHEREUM_RAW:
                EthereumRawFactory ethraw = new EthereumRawFactory();
                ethraw.setConnection(connectorDTO.getConnection());
                ethraw.setName(connectorDTO.getName());
                ethraw.setAuthHeader(connectorDTO.getApiKey());
                ethraw = collaborationFactoryRepository.save(ethraw);
                break;
            case HYPERLEDGER:
                HyperledgerFactory hyperledgerFactory = new HyperledgerFactory();
                hyperledgerFactory.setConnection(connectorDTO.getConnection());
                hyperledgerFactory.setName(connectorDTO.getName());
                hyperledgerFactory = collaborationFactoryRepository.save(hyperledgerFactory);
                break;
            case BFTSMART:
                BftSmartFactory bftSmartFactory = new BftSmartFactory();
                bftSmartFactory.setConnection(connectorDTO.getConnection());
                bftSmartFactory.setName(connectorDTO.getName());
                bftSmartFactory.setUseDefaultKeys(connectorDTO.isUseDefaultKeys());
                bftSmartFactory.setClientPrivateKey(connectorDTO.getClientPrivateKey());
                bftSmartFactory.setPublicKeys(connectorDTO.getPublicKeys());
                bftSmartFactory.setClientPublicKey(connectorDTO.getClientPublicKey());
                bftSmartFactory = collaborationFactoryRepository.save(bftSmartFactory);
                break;
            default:
                throw new RuntimeException("Unknown Collaboration Type");
        }
        return ResponseEntity.ok(null);
    }

    @PreAuthorize("hasRole('admin')")
    @PostMapping("bpmEngine")
    public ResponseEntity<?> createBpmEngine(@RequestBody BPMConnectorDTO connectorDTO) {
        BPMEngineType type = BPMEngineType.valueOf(connectorDTO.getType());
        switch (type) {
            case CAMUNDA_BPM_ENGINE:
                if (connectorDTO.getConnection() == null)
                    throw new RuntimeException("Connection missing");
                CamundaBPMEngineFactory cemf = new CamundaBPMEngineFactory();
                cemf.setCamundaUrl(connectorDTO.getConnection());
                cemf.setName(connectorDTO.getName());
                cemf = bpmEngineFactoryRepository.save(cemf);
                break;
            case MALICIOUS_ENGINE:
                MaliciousEngineFactory mef = new MaliciousEngineFactory();
                mef.setCamundaUrl(connectorDTO.getConnection());
                mef.setName(connectorDTO.getName());
                mef = bpmEngineFactoryRepository.save(mef);
                break;
            default:
                throw new RuntimeException("Unknown BPM Engine Type");
        }
        return ResponseEntity.ok(null);
    }

    @PreAuthorize("hasRole('admin')")
    @PostMapping("communicationModule")
    public ResponseEntity<?> createCommunicationModule(@RequestBody CommunicationConnectorDTO connectorDTO) {
        CommunicationModuleType type = CommunicationModuleType.valueOf(connectorDTO.getType());
        switch (type) {
            case DISCORD:
                if (connectorDTO.getServerId() == null)
                    throw new RuntimeException("Server ID missing");
                if (connectorDTO.getConnection() == null)
                    throw new RuntimeException("Connection missing");
                DiscordAdapterFactory discordAdapterFactory = new DiscordAdapterFactory();
                discordAdapterFactory.setName(connectorDTO.getName());
                discordAdapterFactory.setDiscordServiceUrl(connectorDTO.getConnection());
                discordAdapterFactory.setDiscordServerId(connectorDTO.getServerId());
                discordAdapterFactory = communicationFactoryRepository.save(discordAdapterFactory);
                break;
            case MATRIX:
                MatrixAdapterFactory matrixAdapterFactory = new MatrixAdapterFactory();
                matrixAdapterFactory.setName(connectorDTO.getName());
                matrixAdapterFactory.setMatrixServiceUrl(connectorDTO.getConnection());
                matrixAdapterFactory = communicationFactoryRepository.save(matrixAdapterFactory);
                break;
            default:
                throw new RuntimeException("Unknown Communication Module Type");
        }
        return ResponseEntity.ok(null);
    }


    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("communicationModules")
    public ResponseEntity<?> getCommunicationModules() {
        List<CommunicationFactory> factories = communicationFactoryRepository.findAll();
        GetCommunicationConnectorDTO[] connectors = new GetCommunicationConnectorDTO[factories.size()];
        for (int i = 0; i < factories.size(); i++) {
            CommunicationFactory factory = factories.get(i);
            connectors[i] = new GetCommunicationConnectorDTO();
            connectors[i].setName(factory.getName());
            if (factory instanceof DiscordAdapterFactory) {
                connectors[i].setType(CommunicationModuleType.DISCORD.label);
                connectors[i].setConnection(((DiscordAdapterFactory) factory).getDiscordServiceUrl());
                connectors[i].setServerId(((DiscordAdapterFactory) factory).getDiscordServerId());
            } else if (factory instanceof MatrixAdapterFactory) {
                connectors[i].setType(CommunicationModuleType.MATRIX.label);
                connectors[i].setConnection(((MatrixAdapterFactory) factory).getMatrixServiceUrl());
            } else throw new RuntimeException("Unknown communication factory type");
            connectors[i].setUsed(isCommunicationModuleUsed(factory.getName()));
        }
        return ResponseEntity.ok(connectors);
    }

    private boolean isCommunicationModuleUsed(String name) {
        List<Alliance> alliances = allianceRepository.findAll();
        for (Alliance alliance : alliances)
            if (alliance.getCommunication() != null && alliance.getCommunication().getName().equals(name))
                return true;
        return false;
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("bpmEngines")
    public ResponseEntity<?> getBpmEngines() {
        List<BPMEngineFactory> factories = bpmEngineFactoryRepository.findAll();
        GetConnectorDTO[] connectors = new GetConnectorDTO[factories.size()];
        for (int i = 0; i < factories.size(); i++) {
            BPMEngineFactory factory = factories.get(i);
            connectors[i] = new GetConnectorDTO();
            connectors[i].setName(factory.getName());
            if (factory instanceof CamundaBPMEngineFactory) {
                connectors[i].setType(BPMEngineType.CAMUNDA_BPM_ENGINE.label);
                connectors[i].setConnection(((CamundaBPMEngineFactory) factory).getCamundaUrl());
            } else if (factory instanceof MaliciousEngineFactory)
                connectors[i].setType(BPMEngineType.MALICIOUS_ENGINE.label);
            else throw new RuntimeException("Unknown bpm engine factory type");
            connectors[i].setUsed(isBpmEngineUsed(factory.getName()));
        }
        return ResponseEntity.ok(connectors);
    }

    private boolean isBpmEngineUsed(String name) {
        List<Alliance> alliances = allianceRepository.findAll();
        for (Alliance alliance : alliances) {
            if (alliance.getEngine().getName().equals(name))
                return true;
        }
        return false;
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("collaborations")
    public ResponseEntity<?> getCollaborations() {
        List<CollaborationFactory> factories = collaborationFactoryRepository.findAll();
        GetConnectorDTO[] connectors = new GetConnectorDTO[factories.size()];
        for (int i = 0; i < factories.size(); i++) {
            CollaborationFactory factory = factories.get(i);
            connectors[i] = new GetConnectorDTO();
            connectors[i].setName(factory.getName());
            connectors[i].setConnection(factory.getConnection());
            if (factory instanceof EthereumRawFactory)
                connectors[i].setType(CollaborationType.ETHEREUM_RAW.label);
            else if (factory instanceof HyperledgerFactory)
                connectors[i].setType(CollaborationType.HYPERLEDGER.label);
            else if (factory instanceof BftSmartFactory)
                connectors[i].setType(CollaborationType.BFTSMART.label);
            else throw new RuntimeException("Unknown collaboration factory type");
            connectors[i].setUsed(isCollaborationUsed(factory.getName()));
        }
        return ResponseEntity.ok(connectors);
    }

    public boolean isCollaborationUsed(String name) {
        List<Alliance> alliances = allianceRepository.findAll();
        for (Alliance alliance : alliances)
            if (alliance.getCollaboration().getName().equals(name))
                return true;
        return false;
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("models")
    public ResponseEntity<?> getModels() {
        List<ProcessModelFactory> factories = baseModelFactoryRepository.findAll();
        ModelNameDTO[] nameDtos = new ModelNameDTO[factories.size()];
        for (int i = 0; i < factories.size(); i++) {
            String name = factories.get(i).getName();
            nameDtos[i] = new ModelNameDTO(name, isProcessModelUsed(name));
        }
        return ResponseEntity.ok(nameDtos);
    }

    public boolean isProcessModelUsed(String modelReference) {
        List<Alliance> alliances = allianceRepository.findAll();
        for (Alliance alliance : alliances) {
            if (alliance.getModel().getModelReference().equals(modelReference))
                return true;
        }
        return false;
    }

    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("communicationModule")
    public ResponseEntity<?> deleteCommunicationModule(@RequestParam String name) {
        communicationFactoryRepository.deleteById(name);
        return ResponseEntity.ok(null);
    }

    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("collaboration")
    public ResponseEntity<?> deleteCollaboration(@RequestParam String name) {
        collaborationFactoryRepository.deleteById(name);
        return ResponseEntity.ok(null);
    }

    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("bpmEngine")
    public ResponseEntity<?> deleteBpmEngine(@RequestParam String name) {
        bpmEngineFactoryRepository.deleteById(name);
        return ResponseEntity.ok(null);
    }

    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("model")
    public ResponseEntity<?> deleteProcessModel(@RequestParam String modelReference) {
        baseModelFactoryRepository.deleteById(modelReference);
        return ResponseEntity.ok(null);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("worklist")
    public ResponseEntity<?> getWorklist(@RequestParam String gir) {

        Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow(() -> new RuntimeException());
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(gir)).findFirst().orElseThrow(() -> new RuntimeException());
        List<Task> tasks = dpexService.getWorkList(alliance, instance);

        return ResponseEntity.ok(tasks);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("individualWorklist")
    public ResponseEntity<?> getIndividualWorklist(@RequestParam String gir, @RequestParam String bpmId) {
        Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow(() -> new RuntimeException());
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(gir)).findFirst().orElseThrow(() -> new RuntimeException());
        List<Task> tasks = dpexService.getIndividualWorklist(alliance, instance, bpmId);

        return ResponseEntity.ok(tasks);
    }

    //only bpmn xml for a given gar
    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("bpmnModel")
    public ResponseEntity<?> getBpmnModel(@RequestParam String gar) {
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow();
        CamundaMPModel model = (CamundaMPModel) alliance.getModel();
        return ResponseEntity.ok(model.getBpmnModel());
    }

    //Includes the bpmn xml and the org model for a given model reference
    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("processModel")
    public ResponseEntity<?> getProcessModel(@RequestParam String name) {
        CamundaMPModelFactory factory = (CamundaMPModelFactory) baseModelFactoryRepository.findById(name).orElseThrow();
        GetProcessModelDTO model = new GetProcessModelDTO(new String(factory.getBpmnModel(), StandardCharsets.UTF_8), factory.getOrganizationalModel());
        return ResponseEntity.ok(model);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("potentialTaskExecutors")
    public ResponseEntity<?> getPotentialTaskExecutors(@RequestParam String gir, @RequestParam String task) {
        Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow();
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(gir)).findFirst().orElseThrow();
        EventLog log = instance.getEventLog();
        CamundaMPModel camundaMPModel = (CamundaMPModel) alliance.getModel();
        BPMNModel bpmnModel = BPMNModel.fromString(new String(camundaMPModel.getBpmnModel()));
        String orgModel = camundaMPModel.getOrganizationalModel();
        String[] potentialTaskExecutors = OrgUtils.getPotentialTaskExecutors(task, log, orgModel, bpmnModel);
        return ResponseEntity.ok(potentialTaskExecutors);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @PostMapping("claim")
    public ResponseEntity<?> claimTask(@RequestBody TaskActionDTO taskActionDTO) {
        if (!isDevProfileActive() && !SecurityUtils.hasBpmId(taskActionDTO.getBpmId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User does not have access to the requested bpmId!");
        }
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(taskActionDTO.getGar()).orElseThrow(() -> new RuntimeException());
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(taskActionDTO.getGir())).findFirst().orElseThrow(() -> new RuntimeException());
        User user = OrgUtils.getUserFromBpmId(alliance.getModel(), taskActionDTO.getBpmId());
        dpexService.sendTaskAction(
                alliance,
                instance,
                user,
                Task.builder().activity(taskActionDTO.getTask()).build(),
                new ProcessVariables(taskActionDTO.getProcessVariables()),
                TaskLifeCycleStage.valueOf(taskActionDTO.getTaskLifeCycleStage()));

        return ResponseEntity.ok(null);
    }

    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @PostMapping("complete")
    public ResponseEntity<?> completeTask(@RequestBody TaskActionDTO taskActionDTO) {
        if (!isDevProfileActive() && !SecurityUtils.hasBpmId(taskActionDTO.getBpmId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User does not have access to the requested bpmId!");
        }
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(taskActionDTO.getGar()).orElseThrow(() -> new RuntimeException());
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(taskActionDTO.getGir())).findFirst().orElseThrow(() -> new RuntimeException());
        User user = OrgUtils.getUserFromBpmId(alliance.getModel(), taskActionDTO.getBpmId());
        dpexService.sendTaskAction(
                alliance,
                instance,
                user,
                Task.builder().activity(taskActionDTO.getTask()).build(),
                new ProcessVariables(taskActionDTO.getProcessVariables()),
                TaskLifeCycleStage.COMPLETE);

        return ResponseEntity.ok(null);
    }

    /**
     * Fetches SCI IDs for a given BPM ID, is used by the frontend to determine to which websocket requests
     * it should subscribe to listen for transactions to sign.
     * Returns a set of sciIds if the user has the appropriate access, or a forbidden status otherwise.
     *
     * @param bpmId The BPM ID for which to retrieve sciIds.
     * @return ResponseEntity with either a set of sciIds or a forbidden status message.
     */
    @PreAuthorize("hasRole('dpex_member') or hasRole('admin')")
    @GetMapping("/sciIds/{bpmId}")
    public ResponseEntity<Set<String>> getSciIdsByBpmId(@PathVariable String bpmId) {
        if (!isDevProfileActive() && !SecurityUtils.hasBpmId(bpmId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(Set.of("User does not have access to the requested bpmId!"));
        }
        Set<String> sciIds = new HashSet<>();
        List<ProcessModelFactory> models = baseModelFactoryRepository.findAll();

        for (ProcessModelFactory model : models) {
            Set<User> participants = model.getParticipants();
            for (User user : participants) {
                if (bpmId.equals(user.getBpmId())) {
                    sciIds.add(user.getSciId());
                }
            }
        }
        return ResponseEntity.ok(sciIds);
    }


    /**
     * This REST point is used for the upload of script files
     *
     * @param scriptFile
     * @param gar
     * @return
     */
    @PostMapping(value = "{gar}/script")
    public ResponseEntity<?> uploadScript(@RequestParam("scriptFile") MultipartFile scriptFile, @PathVariable("gar") String gar) {
        List<Alliance> alliances = allianceRepository.findAll();
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).get();
        try {
            ScriptManager.saveFile(scriptFile, alliance.getModel().getModelReference());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok().build();
    }

    /**
     * This is supposed to be called when using the camunda plugins
     * Note: The endpoint expectes the instances localInstanceReference. The reason for that is, that the
     * localInstanceReference is the only reference known by Camunda and therefore the Camunda frontend can
     * only identify the instances by that
     *
     * @param taskActionDTO
     * @return
     */
    @PostMapping("camundaClaim")
    public ResponseEntity<?> camundaClaimTask(@RequestBody CamundaTaskActionDTO taskActionDTO) {
        Alliance alliance = null;
        Instance instance = null;
        List<Alliance> alliances = allianceRepository.findAll();
        for (Alliance a : alliances) {
            for (Instance i : a.getEngine().getInstances()) {
                if (i.getLocalInstanceReference().equals(taskActionDTO.getLir())) {
                    alliance = a;
                    instance = i;
                }
            }
        }
        if (alliance == null || instance == null) {
            return ResponseEntity.notFound().build();
        }
        //Alliance alliance = allianceRepository.findByLocalInstanceReference(taskActionDTO.getLir()).orElseThrow(() -> new RuntimeException());
        User user = OrgUtils.getUserFromBpmId(alliance.getModel(), taskActionDTO.getBpmId());
        dpexService.sendTaskAction(
                alliance,
                instance,
                user,
                Task.builder().activity(taskActionDTO.getTask()).build(),
                new ProcessVariables(taskActionDTO.getProcessVariables()),
                TaskLifeCycleStage.valueOf(taskActionDTO.getTaskLifeCycleStage()));

        return ResponseEntity.ok(null);
    }

	/**
	 * This is supposed to be called when using the camunda plugins. The difference to the ApiControllers complete method,
	 * is the usage of the localInstanceReference. In the ApiController, the first instance matching the globalAllianceReference is used.
	 * @param taskActionDTO
	 * @return
	 */
	@PostMapping("camundaComplete")
	public ResponseEntity<?> camundaCompleteTask(@RequestBody CamundaTaskActionDTO taskActionDTO) {
		Alliance alliance = null;
		Instance instance = null;
		List<Alliance> alliances = allianceRepository.findAll();
		for(Alliance a: alliances){
			for(Instance i: a.getEngine().getInstances()){
				if(i.getLocalInstanceReference().equals(taskActionDTO.getLir())) {
					alliance = a;
					instance = i;
				}
			}
		}
		if(alliance == null || instance == null) {
			return ResponseEntity.notFound().build();
		}
		User user = OrgUtils.getUserFromBpmId(alliance.getModel(), taskActionDTO.getBpmId());
		dpexService.sendTaskAction(
				alliance,
				instance,
				user,
				Task.builder().activity(taskActionDTO.getTask()).build(),
				new ProcessVariables(taskActionDTO.getProcessVariables()),
				TaskLifeCycleStage.COMPLETE);

		return ResponseEntity.ok(null);
	}

	@GetMapping(value="allUsers")
	public ResponseEntity<?> getUsers() {
		List<User> users = userRepository.findAll();
		List<UserDTO> userList = new ArrayList<>();
		for(var u: users){
			if(u.getBpmId().equals("localUserForScriptExecution")){
				continue;
			}
			userList.add(new UserDTO(u));
		}
		return ResponseEntity.ok().body(userList);
	}

	@GetMapping(value="allianceNameInUse")
	public ResponseEntity<?> isAllianceNameInUse(@RequestParam String name){
		var res = allianceRepository.findById(name);
		if(res.isEmpty()){
			return ResponseEntity.ok(false);
		}else{
			return ResponseEntity.ok(true);
		}
	}
}

@Data
@AllArgsConstructor
class ModelNameDTO {
    private String name;
    private boolean used;
}

@Data
@AllArgsConstructor
class GetProcessModelDTO {
    private String bpmnModelXml;
    private String orgModel;
}

@Data
class BPMConnectorDTO {
    @JsonProperty(required = true)
    private String type;

    @JsonProperty(required = false)
    private String connection;

    @JsonProperty(required = true)
    private String name;
}

@Data
class CollaborationConnectorDTO {
    @JsonProperty(required = true)
    private String type;

    @JsonProperty(required = true)
    private String connection;

    @JsonProperty(required = true)
    private String name;

    @JsonProperty(required = false)
    private String apiKey = " ";

    // Necessary for BFT-SMaRt with activated keys.
    @JsonProperty(required = false)
    private boolean useDefaultKeys;
    @JsonProperty(required = false)
    private List<String> publicKeys;
    @JsonProperty(required = false)
    private String clientPrivateKey;
    @JsonProperty(required = false)
    private String clientPublicKey;
}

@Data
class CommunicationConnectorDTO {
    @JsonProperty(required = true)
    private String type;

    @JsonProperty(required = false)
    private String connection;

    @JsonProperty(required = true)
    private String name;

    @JsonProperty(required = false)
    private String serverId;
}

@Data
class GetConnectorDTO {
    private String type;
    private String connection;
    private String name;
    private boolean used;
}

@Data
@EqualsAndHashCode(callSuper = true)
class GetCommunicationConnectorDTO extends GetConnectorDTO {
    private String serverId;
}

@Data
@AllArgsConstructor
class EnumValueDTO {
    private String value;
    private String label;
}

@Data
@AllArgsConstructor
class GetAllianceDTO {
    private String name;
    private String gar;
    private String processModel;
    private ParticipantDTO[] participants;
    private Instance[] instances;
    private String communicationModule;
    private String collaboration;
    private String bpmEngine;
}

@Data
class DeploySciDTO{
	@JsonProperty(required = true)
	private String sciId;

	@JsonProperty(required = true)
	private String connection;

	@JsonProperty(required = false)
	private String apiKey = " ";
}

@Data
class ProcessModelDTO {

    private MultipartFile processModel;
    private String organizationalModel;
    private String autoUpgradeUsers;
}

@Data
@AllArgsConstructor
class AllianceDTO {

    private String name;
    private String GAR;

    private String processModel;

    private String bpmEngine;
    private String collaboration;

    private String communication;

    private String blocksToWait;


}

@Data
class InstantiateDTO {
    private String gar;
    private String bpmId;
    private String name;
    private String variables;
}

@Data
class TaskActionDTO {
    private String gar;
    private String gir;
    private String task;
    private String taskLifeCycleStage;
    private String processVariables;
    private String bpmId;

}

@Data
class CamundaTaskActionDTO {
	private String lir;
	private String task;
	private String taskLifeCycleStage;
	private String processVariables;
	private String bpmId;
}

@Data
class UserDTO {
    private String bpmId;

    public UserDTO(User u) {
        this.bpmId = u.getBpmId();
    }
}
