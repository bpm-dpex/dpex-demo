package ubt.ai4.dpexdemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class DpexDemoApplicationTests {

	@Test
	void contextLoads() {
	}

}
