#!/bin/bash

KC_HOME="/opt/keycloak"

# Start Keycloak in the background
/opt/keycloak/bin/kc.sh start-dev --health-enabled=true --import-realm --hostname-strict-backchannel=false&
KC_PID=$!

echo "Waiting for Keycloak to be ready..."

# Wait for Keycloak's health endpoint to report it's up
until curl -sf http://localhost:8080/health/ready; do
    echo "Waiting for Keycloak to be fully ready..."
    # Check if Keycloak process is still running
    if ! kill -0 $KC_PID > /dev/null 2>&1; then
        echo "Keycloak process has exited unexpectedly."
        exit 1
    fi
     sleep 15
done

echo ""
echo "Keycloak is fully ready."

# Run custom setup script
/opt/keycloak/keycloak-setup.sh

echo "Keycloak setup script execution completed."

# Wait for the Keycloak process to finish
wait $KC_PID

