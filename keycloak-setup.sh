#!/bin/bash

# Variables
REALM="master"
ADMIN_USERNAME="admin"
ADMIN_PASSWORD="adminpassword"
SERVER_URL="http://localhost:8080"
KC_HOME="/opt/keycloak"
CLIENT_ID_DPEx_FRONTEND="dpex-frontend"
CLIENT_ID_REALM_MANAGEMENT="realm-management"
CLIENT_SECRET_REALM_MANAGEMENT="GZWR25Lhz9Tw0aySf0lvzk8MoBledfLR"

# Navigate to the bin directory
cd $KC_HOME/bin || exit

echo "Logging into $SERVER_URL as user $ADMIN_USERNAME of realm $REALM"
# Login
./kcadm.sh config credentials --server $SERVER_URL --realm $REALM --user $ADMIN_USERNAME --password $ADMIN_PASSWORD

# Check if dpex-frontend client already exists
if ! ./kcadm.sh get clients -r $REALM --fields id,clientId | grep -q "\"clientId\" : \"$CLIENT_ID_DPEx_FRONTEND\""; then
    # dpex-frontend client creation
    echo "Creating client $CLIENT_ID_DPEx_FRONTEND..."
    ./kcadm.sh create clients -r $REALM \
      -s clientId="$CLIENT_ID_DPEx_FRONTEND" \
      -s 'redirectUris=["http://localhost:3000/*"]' \
      -s webOrigins='["*"]' \
      -s publicClient=true \
      -s directAccessGrantsEnabled=false \
      -s 'defaultClientScopes=["web-origins","roles","profile","email"]' \
      -s 'optionalClientScopes=["address","phone","offline_access","microprofile-jwt"]'
else
    echo "Client $CLIENT_ID_DPEx_FRONTEND already exists. Skipping creation."
fi

# Check if realm-management client already exists
if ! ./kcadm.sh get clients -r $REALM --fields id,clientId | grep -q "\"clientId\" : \"$CLIENT_ID_REALM_MANAGEMENT\""; then
    # realm-management client creation
    echo "Creating client $CLIENT_ID_REALM_MANAGEMENT..."
    REALM_MANAGEMENT_CLIENT_ID=$(./kcadm.sh create clients -r $REALM \
      -s clientId="$CLIENT_ID_REALM_MANAGEMENT" \
      -s publicClient=false \
      -s bearerOnly=false \
      -s serviceAccountsEnabled=true \
      -s secret="$CLIENT_SECRET_REALM_MANAGEMENT" \
      -s 'redirectUris=["/*"]' \
      -s 'webOrigins=["/*"]' \
      -s directAccessGrantsEnabled=false \
      -i)
else
    echo "Client $CLIENT_ID_REALM_MANAGEMENT already exists. Skipping creation."
fi

# Fetch the ID of the realm-management client
REALM_MANAGEMENT_CLIENT_ID=$(./kcadm.sh get clients -r $REALM -q clientId=$CLIENT_ID_REALM_MANAGEMENT --fields id | jq -r '.[0].id')

# Check if the realm-management client ID was found
if [ ! -z "$REALM_MANAGEMENT_CLIENT_ID" ]; then
    # Fetch the service account user associated with the realm-management client
    SERVICE_ACCOUNT_USER_ID=$(./kcadm.sh get clients/"$REALM_MANAGEMENT_CLIENT_ID"/service-account-user -r $REALM --fields id | jq -r '.id')
    # Ensure SERVICE_ACCOUNT_USER_ID is not empty before proceeding
    if [ ! -z "$SERVICE_ACCOUNT_USER_ID" ]; then
        # Assign roles (admin and create-realm) function
        assign_role() {
            ROLE_NAME=$1
            # Check if role exists
            if ! ./kcadm.sh get roles -r $REALM | jq -e ".[] | select(.name==\"$ROLE_NAME\")" > /dev/null; then
                echo "Creating role $ROLE_NAME..."
                ./kcadm.sh create roles -r $REALM -s name="$ROLE_NAME" -s description="Role that allows $ROLE_NAME actions"
            fi

            # Check if service account already has the role
            if ! ./kcadm.sh get users/"$SERVICE_ACCOUNT_USER_ID"/role-mappings/realm -r $REALM | jq -e ".[] | select(.name==\"$ROLE_NAME\")" > /dev/null; then
                echo "Assigning '$ROLE_NAME' role to service account of realm-management..."
                ./kcadm.sh add-roles --uid "$SERVICE_ACCOUNT_USER_ID" --rolename "$ROLE_NAME" -r $REALM
            else
                echo "Service account of realm-management already has the '$ROLE_NAME' role. Skipping role assignment."
            fi
        }
        # Assign 'admin' role
        assign_role "admin"
        # Assign 'create-realm' role
        assign_role "create-realm"
    else
        echo "Service account user for realm-management client not found."
    fi
else
    echo "Realm-management client ID not found."
fi

set_login_theme_if_necessary() {
    local realm=$1
    local desired_theme="keycloakify-starter"

    local current_login_theme=$(./kcadm.sh get realms/"$realm" --fields loginTheme | jq -r '.loginTheme')

    if [ "$current_login_theme" != "$desired_theme" ]; then
        echo "Setting login theme of realm '$realm' to '$desired_theme'..."
        ./kcadm.sh update realms/"$realm" -s loginTheme=$desired_theme
    else
        echo "Login theme of realm '$realm' is already set to '$desired_theme'. Skipping update."
    fi
}

set_login_theme_if_necessary "$REALM"

cd - || exit

