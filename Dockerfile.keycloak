# Use UBI 9 as the base for the build stage
FROM registry.access.redhat.com/ubi9 AS ubi-micro-build

# Create a new root filesystem
RUN mkdir -p /mnt/rootfs

# Install jq, curl, and dos2unix in the new root filesystem
RUN dnf install --installroot /mnt/rootfs jq curl dos2unix --releasever 9 --setopt install_weak_deps=false --nodocs -y && \
    dnf --installroot /mnt/rootfs clean all

# Start from the Keycloak image
FROM quay.io/keycloak/keycloak:23.0

# Copy the installed jq and curl binaries from the UBI micro build
COPY --from=ubi-micro-build /mnt/rootfs /

# Do standard keycloak install
USER root

COPY ./keycloak-setup.sh /opt/keycloak/
COPY ./custom-entrypoint-keycloak.sh /opt/keycloak/

# Convert line endings from CRLF to LF
RUN dos2unix /opt/keycloak/keycloak-setup.sh
RUN dos2unix /opt/keycloak/custom-entrypoint-keycloak.sh

RUN chmod +x /opt/keycloak/keycloak-setup.sh
RUN chmod +x /opt/keycloak/custom-entrypoint-keycloak.sh

USER 1000

ENTRYPOINT ["/opt/keycloak/custom-entrypoint-keycloak.sh"]
